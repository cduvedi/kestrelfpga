`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 07/02/2013 07:17:06 PM
// Design Name:
// Module Name: SSRSim
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies: blk_mem.
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module SSRSim ();

// value defintions
	`define LEFT	1'd0
	`define RIGHT	1'd1
	`define WRITE	1'd1
	`define READ	1'd0

// signal isntantiations
	reg			CLK;
	reg			RST;
	reg			wea, web;
	reg  [4:0]	addra, addrb;
	reg  [7:0]	dina, dinb;
	wire [7:0]	douta, doutb;
	integer		x;
	parameter integer size = 4;

// module instantiations
	blk_mem SSR (
		.clka	(CLK),		// input clka
		.rsta	(RST),		// input rsta
		.ena	(1),		// input ena
		.wea	(wea),		// input [0 : 0] wea
		.addra	(addra),	// input [4 : 0] addra
		.dina	(dina),		// input [7 : 0] dina
		.douta	(douta),	// output [7 : 0] douta
		.clkb	(CLK),		// input clkb
		.rstb	(RST),		// input rstb
		.enb	(1),		// input enb
		.web	(web),		// input [0 : 0] web
		.addrb	(addrb),	// input [4 : 0] addrb
		.dinb	(dinb),		// input [7 : 0] dinb
		.doutb	(doutb) 	// output [7 : 0] doutb
	);

// clock with 10-cycle period
	always begin
		#5	CLK <= ~CLK;
	end

// testbench
	initial begin
		$display("Starting Simulation: SSRSim");
			CLK <= 1;		// intialize clock
			RST <= 1;		// activate reset
			wea <= `READ;	// intialize to read mode
			web <= `READ;	// intialize to read mode
			addra <= 5'd0;	// intialize addresses
			addrb <= 5'd0;	// intialize addresses
			dina <= 8'd0;	// initialize data inputs
			dinb <= 8'd0;	// intialize data inputs
		#9	RST <= 0;		// release reset

		$monitor($stime, "    [MONITOR] doutA = %0d   doutB = %0d", douta, doutb);

	// write to memory, even addresses through port A, and odd addresses through port B
		$display("-------------------------------------------------------------------");
		for (x = 0; x <= size; x = x + 2) begin
				$display($stime, "    [WRITING] M[%0d] <= %0d from LEFT", x, x);
				write_val(x, x, x);
				$display($stime, "    [WRITING] M[%0d] <= %0d from RIGHT", x+1, x+1);
				write_val(x+1, x+1, x+1);
			#10;
		end

	// set to 0 for debugging purposes
		dina <= 0;
		dinb <= 0;

	// read all written addresses through both ports
		$display("-------------------------------------------------------------------");
		for (x = 0; x <= size; x = x + 1) begin
				$display($stime, "    [READING] LEFT: M[%0d]    RIGHT: M[%0d]", x, x);
				read_val(`LEFT, x);		// they should be equal
				read_val(`RIGHT, x);	// they should be equal
			#10;
		end

	// read opposite addresses from each port
		$display("-------------------------------------------------------------------");
		for (x = 0; x <= size; x = x + 1) begin
				$display($stime, "    [READING] LEFT: M[%0d]    RIGHT: M[%0d]", x, size-x);
				read_val(`LEFT, x);
				read_val(`RIGHT, size-x);
			#10;
		end

		#20	$display("-------------------------------------------------------------------");
		$display("Ending Simulation: SSRSim");
		$finish();
	end

// tasks
	task write_val;
		input		side;
		input [4:0]	addr;
		input [7:0]	din;
		begin
			if (side == `LEFT) begin
				addra <= addr;
				dina <= din;
				wea <= `WRITE;
			end else begin
				addrb <= addr;
				dinb <= din;
				web <= `WRITE;
			end
		end
	endtask: write_val

	task read_val;
		input		side;
		input [4:0]	addr;
		begin
			if (side == `LEFT) begin
				addra <= addr;
				wea <= `READ;
			end else begin
				addrb <= addr;
				web <= `READ;
			end
		end
	endtask: read_val

endmodule: SSRSim
