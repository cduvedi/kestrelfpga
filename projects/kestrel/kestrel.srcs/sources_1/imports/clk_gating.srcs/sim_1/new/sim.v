`timescale 1ns / 1ps

module sim ();

// signal instantiations
	reg			CLK;
	reg			RST_L;
	reg	[7:0]	led;

	kestrel System (
		.CLK	(CLK),		// input  wire			CLK,
		.RST_L	(RST_L),	// input  wire			RST_L,
		.led	(led)		// output reg  [7:0]	led
	);

	always begin
		#5	CLK <= ~CLK;
	end

// testbench
	initial begin
		$display("Starting Simulation: sim");
			CLK <= 1;		// intialize clock
			RST_L <= 0;		// activate reset
		#19	RST_L <= 1;		// release reset

		#200 $display("Ending Simulation: sim");
		$finish();
	end

endmodule: sim