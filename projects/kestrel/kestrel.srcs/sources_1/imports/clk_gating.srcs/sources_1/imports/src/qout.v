`timescale 1ns / 1ps

/**
* Output Queue
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly. (p.82,85)
*
*/

module qout (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire			I_CBS_LOAD,
	input  wire			I_CBS_SLEFT,
	input  wire			I_DATA_WRITE,
	input  wire			I_FIFO_OUT,
	input  wire  [7:0]	SCR_OUT,
	input  wire  [7:0]	BS_OUT,

	output reg   [7:0]	out,
	output wire			qout_int
);

	wire	qout_sel = I_CBS_LOAD | I_CBS_SLEFT;
	wire	write_en = I_DATA_WRITE | I_FIFO_OUT;
	wire	data_out = qout_sel ? BS_OUT : SCR_OUT;

	wire	full;
	assign	qout_int = full & write_en;	// TODO: or (empty and host read)

	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			out <= 8'h00;
		end else if (write_en) begin
			out <= data_out;
		end
	end

	// 8x4096
	queue_fifo Qout (
	 	.clk		(CLK),
		.rst		(~RST_L),
		.din		(data_out),
		.wr_en		(write_en),
		.rd_en		(1'b0),			// (unused for now)
		.dout		(),
		.full		(full),
		.empty		()
	);

endmodule: qout