`timescale 1ns / 1ps

/*
RTL Macro Resources
===================

Macro type     Flop  LUT  BRAM  DSP48
-------------------------------------
Bitwise Logic  0     1    0     0
Multiplexers   0     168  0     0
Storage        256   0    0     0
Total          256   169  0     0


RTL Primitive Statistics
========================

Primitive type       Count
--------------------------
1-bit Bitwise Logic  1
8-bit Multiplexers   3
1-bit Storage        256
*/

module ssr (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire  [4:0]	i_opA,			// register selection
	input  wire  [4:0]	i_opC,
	input  wire  [5:0]	i_dest,
	input  wire			nop,			// don't store anything
	input  wire  [7:0]	result_left,	// data from PE to SSR
	input  wire  [7:0]	result_right,
	input  wire			mask_left,		// write enable
	input  wire			mask_right,		// write enable

	output wire  [7:0]	opA,			// data from SSR to PE
	output wire  [7:0]	opC
);

// Memory component
	reg  [7:0]	ssr	[0:31];

// Reading
	assign opA = ssr[i_opA];
	assign opC = ssr[i_opC];

// Writing
	wire 		from_left	= i_dest[5];
	wire  [7:0]	data_in		= from_left ? result_left : result_right;
	wire		write_en	= ((mask_left & from_left) | (mask_right & ~from_left)) & ~nop;

// FF elements
	integer i;
	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			for (i = 0; i<32; i = i+1) begin
				ssr[i] <= 8'h00;
			end
		end else if (write_en) begin
			ssr[i_dest[4:0]] <= data_in;
		end
	end

endmodule
