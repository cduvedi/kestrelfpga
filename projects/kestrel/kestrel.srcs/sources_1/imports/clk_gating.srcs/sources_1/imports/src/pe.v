`timescale 1ns / 1ps

/**
* Processing Element (PE)
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (p.12-13)
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module pe (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire [52:0]	instruction,
	input  wire  [7:0]	opAFromLeft,
	input  wire  [7:0]	opAFromRight,
	input  wire  [7:0]	opCFromLeft,
	input  wire  [7:0]	opCFromRight,

	output wire  [7:0]	result,
	output wire			pe_wor,
	output wire			mask
);

// PE instruction fields
	// ??? is it better to keep the signals inside or outside the PEs
	wire [7:0]	i_imm	= instruction[7:0];		// a_imm
	wire [5:0]	i_dest	= instruction[13:8];	// dest_reg
	wire [5:0]	i_opC	= instruction[19:14];	// opC_reg
	wire [5:0]	i_opA	= instruction[25:20];	// opA_reg
	wire [2:0]	i_opB	= instruction[28:26];	// opB_sel
	wire		i_wr	= instruction[29];		// sram_w
	wire		i_rd	= instruction[30];		// sram_r
	wire [1:0]	i_rm	= instruction[32:31];	// res_mx
	wire [3:0]	i_bit	= instruction[36:33];	// bit_shift
	wire [4:0]	i_fb	= instruction[41:37];	// flag_bus
	wire		i_finv	= instruction[42];		// fi
	wire		i_lc	= instruction[43];		// lc
	wire		i_mp	= instruction[44];		// mp
	wire		i_ci	= instruction[45];		// ci
	wire [4:0]	i_func	= instruction[50:46];	// alu_func
	wire		i_force	= instruction[51];		// frce
	wire		i_nop	= instruction[52];		// controller nop

// Signal instantiations
	wire [7:0]	opA, opB, opC;
	wire [7:0]	alu_result;
	wire		alu_flag;
	wire [7:0]	mult_result, mhi;
	wire		cmp_flag, eq_flag;
	wire [7:0]	mdr;
	wire [7:0]	bs;
	wire		bs_flag;
	wire		fb;

// Module instantiations

	opsel OperandSelector (
		.opAFromLeft	(opAFromLeft),			// input  wire [7:0]	opAFromLeft,
		.opCFromLeft	(opCFromLeft),			// input  wire [7:0]	opCFromLeft,
		.opAFromRight	(opAFromRight),			// input  wire [7:0]	opAFromRight,
		.opCFromRight	(opCFromRight),			// input  wire [7:0]	opCFromRight,
		.fromRightA		(i_opA[5]),				// input  wire			fromRightA,
		.fromRightC		(i_opC[5]),				// input  wire			fromRightC,
		.i_opB			(i_opB),				// input  wire [2:0]	i_opB,
		.mdr			(mdr),					// input  wire [7:0]	mdr,
		.mhi			(mhi),					// input  wire [7:0]	mhi,
		.bs				(bs),					// input  wire [7:0]	bs,
		.i_imm			(i_imm),				// input  wire [7:0]	i_imm,
		.opA			(opA),					// output wire [7:0]	opA,
		.opB			(opB),					// output reg  [7:0]	opB,
		.opC			(opC)					// output wire [7:0]	opC
	);

	alu ALU (
		.CLK			(CLK),					// input  wire      	CLK,
		.RST_L			(RST_L),				// input  wire      	RST_L,
		.flag_sel		(i_fb[1:0]),			// input  wire [1:0]	flag_sel,
		.i_lc			(i_lc),					// input  wire      	i_lc,
		.i_mp			(i_mp),					// input  wire      	i_mp,
		.i_ci			(i_ci),					// input  wire      	i_ci,
		.i_func			(i_func),				// input  wire [4:0]	i_func,
		.opA			(opA),					// input  wire [7:0]	opA,
		.opB			(opB),					// input  wire [7:0]	opB,
		.alu_result		(alu_result),			// output wire [7:0]	alu_result,
		.alu_flag		(alu_flag)				// output wire			alu_flag
	);

	mult Multiplier (
		.CLK			(CLK),					// input  wire			CLK,
		.RST_L			(RST_L),				// input  wire			RST_L,
		.i_func			(i_func),				// input  wire  [4:0]	i_func,
		.opA			(opA),					// input  wire  [7:0]	opA,
		.opB			(opB),					// input  wire  [7:0]	opB,
		.opC			(opC),					// input  wire  [7:0]	opC,
		.mask			(mask),					// input  wire			mask,
		.mult_result	(mult_result),			// output wire  [7:0]	mult_result,
		.mhi			(mhi)					// output reg   [7:0]	mhi
	);

	cmp Comparator (
		.CLK			(CLK),					// input				CLK,
		.RST_L			(RST_L),				// input				RST_L,
		.i_fb			(i_fb),					// input  wire  [4:0]	i_fb,
		.opC			(opC),					// input  wire  [7:0]	opC,
		.alu_result		(alu_result),			// input  wire  [7:0]	alu_result,
		.mask			(mask),					// input  wire			mask,
		.cmp_flag		(cmp_flag),				// output reg			cmp_flag,
		.eq_flag		(eq_flag)				// output reg			eq_flag
	);

	sram SRAM (
		.CLK			(CLK),					// input  wire			CLK,
		.RST_L			(RST_L),				// input  wire			RST_L,
		.i_imm			(i_imm),				// input  wire  [7:0]	i_imm,
		.i_wr			(i_wr),					// input  wire			i_wr,
		.i_rd			(i_rd),					// input  wire			i_rd,
		.opC			(opC),					// input  wire  [7:0]	opC,
		.result			(result),				// input  wire  [7:0]	result,
		.mask			(mask),					// input  wire			mask,
		.mdr			(mdr)					// output wire  [7:0]	mdr
	);

	resultSel ResultSelector (
		.flag			(fb),					// input  wire			flag,
		.i_rm			(i_rm),					// input  wire  [1:0]	i_rm,
		.i_lc			(i_lc),					// input  wire			i_lc,
		.i_ci			(i_ci),					// input  wire			i_ci,
		.mult_result	(mult_result),			// input  wire  [7:0]	mult_result,
		.alu_result		(alu_result),			// input  wire  [7:0]	alu_result,
		.opC			(opC),					// input  wire  [7:0]	opC,
		.result			(result)				// output reg   [7:0]	result
	);

	bs BitShifter (
		.CLK			(CLK),					// input  wire		 	CLK,
		.RST_L			(RST_L),				// input  wire			RST_L,
		.i_bit			(i_bit),				// input  wire [3:0]	i_bit,
		.flag_sel		(i_fb[1:0]),			// input  wire [1:0]	flag_sel,
		.i_force		(i_force),				// input  wire			i_force,
		.i_nop			(i_nop),				// input  wire			i_nop,
		.fb				(fb),					// input  wire			fb,
		.result			(result),				// input  wire [7:0]	result,
		.bs				(bs),					// output reg  [7:0]	bs,
		.mask			(mask),					// output reg			mask,
		.bs_flag		(bs_flag),				// output reg			bs_flag
		.pe_wor			(pe_wor)				// output wire			pe_wor
	);

	flagMUX FlagMUX (
		.i_fb			(i_fb[4:2]),			// input  wire  [4:2]	i_fb,
		.i_finv			(i_finv),				// input  wire			i_finv,
		.cmp_flag		(cmp_flag),				// input  wire			cmp_flag,
		.bs_flag		(bs_flag),				// input  wire			bs_flag,
		.alu_flag		(alu_flag),				// input  wire			alu_flag,
		.eq_flag		(eq_flag),				// input  wire			eq_flag,
		.flag			(fb)					// output reg			flag
	);

endmodule: pe