`timescale 1ns / 1ps

/**
* Arithmetic Logic Unit (ALU)
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.5)
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module alu (
	input  wire      	CLK,
	input  wire      	RST_L,
	input  wire [1:0]	flag_sel,	// (fSel)
	input  wire      	i_lc,		// lc
	input  wire      	i_mp,		// mp
	input  wire      	i_ci,		// ci
	input  wire [4:0]	i_func,		// alu_func
	input  wire [7:0]	opA,
	input  wire [7:0]	opB,

	output wire [7:0]	alu_result,
	output reg			alu_flag
);

// signal instantiations and assignments
	reg			carry_latch;	// carry output of the previous operation
	reg  [7:0]	alu_opA;		// ALU input A (operand A)
	reg  [7:0]	alu_opB;		// ALU input B (operand B)
	reg  [7:0]	logic_result;	// logical ALU output (result)

	wire      	alu_ci = i_ci | (carry_latch & i_mp);		// ALU carry-in, TODO: confirm

	// adder
	wire [8:0]	add_result = alu_opA + alu_opB + alu_ci;	// adder output
	wire		add_co = add_result[8];

	// ALU outputs
	wire		alu_co = i_lc ? add_co : 1'b0;				// ALU carry-out
	wire		alu_ats = i_lc ? add_result[7] : 1'b0;		// TODO: confirm

	assign alu_result = i_lc ? add_result[7:0] : logic_result;

// opcode list
	`define AND		5'b00111
	`define INVERTA	5'b01010
	`define INVERTB	5'b01100
	`define MOVEA	5'b00101
	`define MOVEB	5'b00011
	`define NAND	5'b01000
	`define NOP		5'b00101
	`define NOR		5'b01110
	`define OR		5'b00001
	`define XNOR	5'b00110
	`define XOR		5'b01001
	`define ADD		5'b10001
	`define ADDAA	5'b10101
	`define ADDBB	5'b10011
	`define ADDAZ	5'b00101
	`define ADDBZ	5'b00011
	`define ADDZZ	5'b01111
	`define SUBAB	5'b10100
	`define SUBBA	5'b10010
	`define SUBAZ	5'b11101
	`define SUBBZ	5'b11011
	`define SUBZA	5'b01100
	`define SUBZB	5'b01010
	`define SUBZZ	5'b00000

//synthesis translate_off
	reg [39:0] op_ascii;
	always @* begin
		if (i_lc) begin
			case (i_func)
				`ADD:		op_ascii = "ADD  ";
				`ADDAA:		op_ascii = "ADDAA";
				`ADDBB:		op_ascii = "ADDBB";
				`ADDAZ:		op_ascii = "ADDAZ";
				`ADDBZ:		op_ascii = "ADDBZ";
				`ADDZZ:		op_ascii = "ADDZZ";
				`SUBAB:		op_ascii = "SUBAB";
				`SUBBA:		op_ascii = "SUBBA";
				`SUBAZ:		op_ascii = "SUBAZ";
				`SUBBZ:		op_ascii = "SUBBZ";
				`SUBZA:		op_ascii = "SUBZA";
				`SUBZB:		op_ascii = "SUBZB";
				`SUBZZ:		op_ascii = "SUBZZ";
				default:	op_ascii = " ??? ";
			endcase
		end else begin
			case (i_func)
				`AND:		op_ascii = "AND  ";
				`INVERTA:	op_ascii = "INVA ";
				`INVERTB:	op_ascii = "INVB ";
				`NOP:		op_ascii = "NOP  ";
				`MOVEA:		op_ascii = "MOVEA";
				`MOVEB:		op_ascii = "MOVEB";
				`NAND:		op_ascii = "NAND ";
				`NOR:		op_ascii = "NOR  ";
				`OR:		op_ascii = "OR   ";
				`XNOR:		op_ascii = "XNOR ";
				`XOR:		op_ascii = "XOR  ";
				default:	op_ascii = " ??? ";
			endcase
		end
	end
//synthesis translate_on

// first select the two adder inputs
	always @* begin
		case (i_func)
			`ADD:		alu_opA = opA;
			`ADDAA:		alu_opA = opA;
			`ADDBB:		alu_opA = opB;
			`ADDAZ:		alu_opA = opA;
			`ADDBZ:		alu_opA = opB;
			`ADDZZ:		alu_opA = 8'h00;
			`SUBAB:		alu_opA = opA;
			`SUBBA:		alu_opA = opB;
			`SUBAZ:		alu_opA = opA;
			`SUBBZ:		alu_opA = opB;
			`SUBZA:		alu_opA = 8'h00;
			`SUBZB:		alu_opA = 8'h00;
			`SUBZZ:		alu_opA = 8'h00;
			default:	alu_opA = 8'h00;
		endcase
	end

	always @* begin
		case (i_func)
			`ADD:		alu_opB = opB;
			`ADDAA:		alu_opB = opA;
			`ADDBB:		alu_opB = opB;
			`ADDAZ:		alu_opB = 8'h00;
			`ADDBZ:		alu_opB = 8'h00;
			`ADDZZ:		alu_opB = 8'h00;
			`SUBAB:		alu_opB = ~opB;
			`SUBBA:		alu_opB = ~opA;
			`SUBAZ:		alu_opB = 8'hff;
			`SUBBZ:		alu_opB = 8'hff;
			`SUBZA:		alu_opB = ~opA;
			`SUBZB:		alu_opB = ~opB;
			`SUBZZ:		alu_opB = 8'hff;
			default:	alu_opB = 8'h00;
		endcase
	end

// logic
	always @* begin
		case (i_func)
			`AND:		logic_result = opA & opB;
			`INVERTA:	logic_result = ~opA;
			`INVERTB:	logic_result = ~opB;
			`MOVEA:		logic_result = opA;
			`MOVEB:		logic_result = opB;
			`NAND:		logic_result = ~(opA & opB);
			`NOR:		logic_result = ~(opA | opB);
			`NOP:		logic_result = 8'h00;
			`OR:		logic_result = opA | opB;
			`XNOR:		logic_result = ~(opA ^ opB);
			`XOR:		logic_result = opA ^ opB;
			default:	logic_result = 8'h00;
		endcase
	end

// store carry
	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			carry_latch <= 1'b0;
		end else begin
			carry_latch <= add_co;
		end
	end

// ALU Flag MUX
	always @* begin
		case (flag_sel)
			2'b00: alu_flag = alu_ats;		// ats (true sign bit)
			2'b01: alu_flag = alu_co;		// aCout (carry out)
			2'b10: alu_flag = carry_latch;	// cLatch (previous carry)
			2'b11: alu_flag = 1'b0;
		endcase
	end

endmodule