`timescale 1ns / 1ps

module gOutput (
	input  wire			CLK,
	input  wire			RST,
	input  wire [8:0]	dina,
	input  wire [8:0]	dinb,

	output wire [8:0]	douta,
	output wire [8:0]	doutb
);

// signal instantiations
	reg   [4:0]	pc;
	reg			en;

// custom ip instantiation, 32x64bit
	output_bram OutputMemory (
		.clka		(CLK), // input clka
		.rsta		(RST), // input rsta
		.ena		(en), // input enb
		.wea		(en), // input [0 : 0] wea
		.addra		({1'b0,pc}), // input [5 : 0] addra
		.dina		(dina), // input [8 : 0] dina
		.douta		(douta), // output [8 : 0] douta
		.clkb		(CLK), // input clkb
		.rstb		(RST), // input rstb
		.enb		(en), // input enb
		.web		(en), // input [0 : 0] web
		.addrb		({1'b1,pc}), // input [5 : 0] addrb
		.dinb		(dinb), // input [8 : 0] dinb
		.doutb		(doutb) // output [8 : 0] doutb
	);

// logic
	always @ (posedge CLK) begin
		if (RST) begin
			pc <= 5'd0;
			en <= 1'b0;
		end else begin
			pc <= pc + 5'd1;
			en <= 1'b1;
		end
	end



endmodule
