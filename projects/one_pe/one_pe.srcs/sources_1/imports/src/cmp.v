/* cmp.v
	Comparator
	Andrew W. Hill
	August 2006

	Supports minc,maxc,sminc,smaxc.  Does not support mod256
	operations.
		                                                   		*/

module cmp (
	input				CLK,
	input				RST,
	input  wire [7:0]	inputA,		// two in
	input  wire [7:0]	inputB,
	input  wire [1:0]	resmx,
	input  wire			max,		// 0: return max, 1: return min
	input  wire			sign,		// 0: inputs are unsigned, 1: inputs are signed
	input  wire			on,

	output reg  [7:0]	result,		// one out
	output wire			eq,
	output wire			min,
	output reg			eqLatch,
	output reg			minLatch
);

// signal instantiations
	wire [8:0] tmpA, tmpB;		// extra bit used for signed comparisons
	reg  [8:0] tmpresult;
	wire [8:0] cmptmp;

// logic
	assign tmpA = {inputA[7] & sign, inputA};
	assign tmpB = {inputB[7] & sign, inputB};

	always @* begin
		if (min) begin
			tmpresult = max ? tmpA : tmpB;  // B>A
		end else begin
			tmpresult = max ? tmpB : tmpA;  // A>B
		end
	end

	always @* begin
		case(resmx)
			2'b10:   result = inputB;
			2'b11:   result = tmpresult[7:0];
			default: result = inputA;
		endcase
	end

	assign cmptmp = tmpA - tmpB;
	assign eq = &cmptmp;
	assign min = cmptmp[8];	// 0: A>B, 1: B>A

	always @ (posedge CLK) begin
		if (RST) begin
			eqLatch <= 1'b0;
			minLatch <= 1'b0;
		end else if (on) begin
			eqLatch <= eq;
			minLatch <= min;
		end else begin
			eqLatch <= eqLatch;
			minLatch <= minLatch;
		end
	end

//synthesis translate_off
	reg [31:0] op_ascii;
	always @* begin
		case({resmx,max,sign})
			4'b1000: op_ascii = "OP C";
			4'b1001: op_ascii = "OP C";
			4'b1010: op_ascii = "OP C";
			4'b1011: op_ascii = "OP C";
			4'b1110: op_ascii = "MIN ";
			4'b1111: op_ascii = "SMIN";
			4'b1100: op_ascii = "MAX ";
			4'b1101: op_ascii = "SMAX";
			default: op_ascii = "ALU ";
		endcase
	end
//synthesis translate_on

endmodule