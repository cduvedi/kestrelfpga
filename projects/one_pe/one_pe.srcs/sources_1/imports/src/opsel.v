`timescale 1ns / 1ps

/* opsel.v
	Selects operands A,B,C
	Andrew W. Hill
	October 2006

	This takes the instruction, SSR data, and data from special
	registers, and transforms it into ops A,B and C.
																*/

module opsel(
	input  wire [7:0]	inLA,
	input  wire [7:0]	inLC,
	input  wire [7:0]	inRA,
	input  wire [7:0]	inRC,
	input  wire			fromRightA,	// 0:left, 1:right
	input  wire			fromRightC,	// 0:left, 1:right
	input  wire [2:0]	opBsel,
	input  wire [7:0]	mdr,
	input  wire [7:0]	mhi,
	input  wire [7:0]	bs,
	input  wire [7:0]	immediate,

	output wire [7:0]	operandA,
	output reg  [7:0]	operandB,	// nonregister
	output wire [7:0]	operandC
);

// Operand A,C Selection
// Selects whether to use the right or left inputs.
	assign operandA = fromRightA ? inRA : inLA;
	assign operandC = fromRightC ? inRC : inLC;

// Operand B Selection
// The top two bits indicate the source, the bottom bit indicates sign extension
// This is not true for 110 and 111, which are BS and Imm, respectively.  I had
// this explicitly masked at one point, but this seems to optimize better.
	always @ * begin
		case (opBsel)
			3'b000: operandB = operandC;			// opbreg
			3'b001: operandB = {8{operandC[7]}};	// opbsreg
			3'b010: operandB = mdr;					// mdr
			3'b011: operandB = {8{mdr[7]}};			// smdr
			3'b100: operandB = mhi;					// mhi
			3'b101: operandB = {8{mhi[7]}};			// smhi
			3'b110: operandB = bs;					// bs
			3'b111: operandB = immediate;			// imm
		endcase
	end

endmodule