/* bs.v
	Bitshifter
	Andrew W. Hill
	October 2006

		                                                   		*/

module bs (
	input  wire		 	CLK,
    input  wire			RST,
	input  wire [3:0]	opcode,
	input  wire			flagin,
	input  wire [7:0]	resultin,
	output reg  [7:0]	bitshifter,
	output reg			maskflag
);

// signal instantiations
	reg [7:0]	newbitshifter;
	reg			newmaskflag;

// opcode list
	`define BSAND		4'b0101
	`define BSCLEAR		4'b1000
	`define BSCLEARM	4'b1001
	`define BSCONDLATCH	4'b1101
	`define BSCONDLEFT	4'b0011
	`define BSCONDRIGHT	4'b1111
	`define BSFLAGMASK	4'b0010
	`define BSLATCH		4'b1100
	`define BSNOT		4'b0110
	`define BSNOTMASK	4'b0001
	`define BSOR		4'b0100
	`define BSPOP		4'b1011
	`define BSPOPNOT	4'b1010
	`define BSPUSH		4'b1110
	`define BSSET		4'b0111

//synthesis translate_off
	reg [39:0]	op_ascii;
	always @* begin
		case (opcode)
			`BSAND:			op_ascii = "AND  ";
			`BSCLEAR:		op_ascii = "CLEAR";
			`BSCLEARM:		op_ascii = "CLRM ";
			`BSCONDLATCH:	op_ascii = "CLTCH";
			`BSCONDLEFT:	op_ascii = "CONDL";
			`BSCONDRIGHT:	op_ascii = "CONDR";
			`BSFLAGMASK:	op_ascii = "FLAGM";
			`BSLATCH:		op_ascii = "LATCH";
			`BSNOT:			op_ascii = "NOT  ";
			`BSNOTMASK:		op_ascii = "NOTM ";
			`BSOR:			op_ascii = "OR   ";
			`BSPOP:			op_ascii = "POP  ";
			`BSPOPNOT:		op_ascii = "POPNT";
			`BSPUSH:		op_ascii = "PUSH ";
			`BSSET:			op_ascii = "SET  ";
			default:		op_ascii = "     ";
		endcase
	end
//synthesis translate_on

// logic bitshifter
	always @* begin
		case (opcode)
			`BSAND:			newbitshifter = {bitshifter[7] | flagin, bitshifter[6:0]};
			`BSCLEAR:		newbitshifter = 8'h00;
			`BSCLEARM:		newbitshifter = 8'h00;
			`BSCONDLATCH:	newbitshifter = maskflag ? bitshifter : resultin;
			`BSCONDLEFT:	newbitshifter = maskflag ? bitshifter : {bitshifter[6:0],flagin};
			`BSCONDRIGHT:	newbitshifter = maskflag ? bitshifter : {flagin,bitshifter[7:1]};
			`BSFLAGMASK:	newbitshifter = bitshifter;
			`BSLATCH:		newbitshifter = resultin;
			`BSNOT:			newbitshifter = {~bitshifter[7], bitshifter[6:0]};		// complement MSB
			`BSNOTMASK:		newbitshifter = bitshifter;
			`BSOR:			newbitshifter = {bitshifter[7] & flagin, bitshifter[6:0]};
			`BSPOP:			newbitshifter = {bitshifter[6:0],1'b0};
			`BSPOPNOT:		newbitshifter = {~bitshifter[6],bitshifter[5:0],1'b0};
			`BSPUSH:		newbitshifter = {flagin, bitshifter[7:1]};
			`BSSET:			newbitshifter = {flagin, bitshifter[6:0]};
			default:		newbitshifter = bitshifter;
		endcase
	end

	always @ (posedge CLK) begin
		if (RST) begin
			bitshifter <= 8'h00;
		end else begin
			bitshifter <= newbitshifter;
		end
	end

// logic maskflag
	always @* begin
			case (opcode)
				`BSAND:			newmaskflag = ~|newbitshifter;
				`BSCLEAR:		newmaskflag = maskflag;
				`BSCLEARM:		newmaskflag = 1'b1;
				`BSCONDLATCH:	newmaskflag = maskflag;
				`BSCONDLEFT:	newmaskflag = maskflag;
				`BSCONDRIGHT:	newmaskflag = maskflag;
				`BSFLAGMASK:	newmaskflag = flagin;
				`BSLATCH:		newmaskflag = ~|newbitshifter;
				`BSNOT:			newmaskflag = ~|newbitshifter;
				`BSNOTMASK:		newmaskflag = ~maskflag;
				`BSOR:			newmaskflag = ~|newbitshifter;
				`BSPOP:			newmaskflag = ~|newbitshifter;
				`BSPOPNOT:		newmaskflag = ~|newbitshifter;
				`BSPUSH:		newmaskflag = ~|newbitshifter;
				`BSSET:			newmaskflag = ~|newbitshifter;
				default:		newmaskflag = maskflag;
		endcase
	end

	always @ (posedge CLK) begin
		if (RST) begin
			maskflag <= 1'b1;
		end else begin
			maskflag <= newmaskflag;
		end
	end


endmodule