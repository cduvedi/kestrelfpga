/* bs.v
	Bitshifter
	Andrew W. Hill
	October 2006

	Note that this unit is basically built behaviourally.
	This design will synthesize, however unless XST is
	significantly cleverer than I give it credit for, it will
	be quite large and not particularly fast.
	However, it is sufficient to verify the initial design.

		                                                   		*/

module flag (
	input  wire [4:0]	flagbus,
	input  wire			ALUco,
	input  wire			ALUcts,
	input  wire			CMPbo,
	input  wire			CMPco,
	input  wire			CMPmsb,
	input  wire			CMPcts,
	input  wire [7:0]	bs,
	input  wire			eq,
	input  wire			min,
	input  wire			eqLatch,
	input  wire			minLatch,
	input  wire			worin,

	output reg			flag
);


// opcode list
	`define FBACO		5'b01101
	`define FBATS		5'b01100
	`define FBBS0		5'b01001
	`define FBBS7		5'b01010
	`define FBBSNOR		5'b01000
	`define FBCBO		5'b00100
	`define FBCLATCH	5'b01110
	`define FBCMSB		5'b00110
	`define FBCTS		5'b00101
	`define FBEQ		5'b10000
	`define FBEQLATCH	5'b10001
	`define FBMINLATCH	5'b00011
	`define FBWOR		5'b01011
	`define LTC			5'b00000

// logic
	always @* begin
		case (flagbus)
			`FBACO:			flag = ALUco;
			`FBATS:			flag = ALUcts;
			`FBBS0:			flag = bs[0];
			`FBBS7:			flag = bs[7];
			`FBBSNOR:		flag = ~|bs;
			`FBCBO:			flag = CMPbo;
			`FBCLATCH:		flag = CMPco;
			`FBCMSB:		flag = CMPmsb;
			`FBCTS:			flag = CMPcts;
			`FBEQ:			flag = eq;
			`FBEQLATCH:		flag = eqLatch;
			`FBMINLATCH:	flag = minLatch;
			`FBWOR:			flag = worin;
			`LTC:			flag = min;
			default:		flag = 1'b0;		// arbitrary
		endcase
	end

//synthesis_translate off
	reg [39:0]	flagbus_ascii;
	always @* begin
		case (flagbus)
			`FBACO:			flagbus_ascii = "A CO ";
			`FBATS:			flagbus_ascii = "A TS ";
			`FBBS0:			flagbus_ascii = "BS 0 ";
			`FBBS7:			flagbus_ascii = "BS 7 ";
			`FBBSNOR:		flagbus_ascii = "BSNOR";
			`FBCBO:			flagbus_ascii = "C BO ";
			`FBCLATCH:		flagbus_ascii = "C L  ";
			`FBCMSB:		flagbus_ascii = "C MSB";
			`FBCTS:			flagbus_ascii = "C TS ";
			`FBEQ:			flagbus_ascii = "EQ   ";
			`FBEQLATCH:		flagbus_ascii = "EQ L ";
			`FBMINLATCH:	flagbus_ascii = "MIN L";
			`FBWOR:			flagbus_ascii = "WOR  ";
			`LTC:			flagbus_ascii = "LTC  ";
			default:		flagbus_ascii = "?????";
		endcase
	end
//synthesis_translate on

endmodule