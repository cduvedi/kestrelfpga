/* result.v
	Selects the PE Result
	Andrew W. Hill
	October 2006

	Yup, just a mux. NOTE: Will contain BS functionality later.
		                                                   		*/

// 2:1 MUX
module result (
	input  wire [7:0]	inputA,
	input  wire [7:0]	inputB,
	input  wire		 	sel,
	output wire [7:0]	result
);

	assign result = sel ? inputA : inputB;

endmodule