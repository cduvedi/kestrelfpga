/* alu.v
	Arith/Logic Unit
	Andrew W. Hill
	August 2006

	Note that this unit is basically built behaviourally.
	This design will synthesize, however unless XST is
	significantly cleverer than I give it credit for, it will
	be quite large and not particularly fast.
	However, it is sufficient to verify the initial design.
		                                                   		*/

module alu (
	input  wire      	CLK,
	input  wire      	RST,
	input  wire [4:0]	opcode,
	input  wire      	lc,
	input  wire [7:0]	operandA,
	input  wire [7:0]	operandB,
	input  wire      	bc,				// borrow/carry
	input  wire      	mp,				// multiprecision mode
	output wire [7:0]	result,
	output wire      	co
);

// signal instantiations
	reg			multiprecisionCarry;	// is a memory element

	reg  [7:0]	ALUinA;
	reg  [7:0]	ALUinB;
	reg  [7:0]	lResult;
	wire [8:0]	tmpResult;
	wire      	ci;

// result selector
	assign result = lc ? tmpResult[7:0] : lResult;
	assign co     = lc ? tmpResult[8]   : 1'b0;			// dont spawn carry on logical ops

// opcode list
	`define AND		5'b00111
	`define INVERTA	5'b01010
	`define INVERTB	5'b01100
	`define MOVEA	5'b00101
	`define MOVEB	5'b00011
	`define NAND	5'b01000
	`define NOP		5'b00101
	`define NOR		5'b01110
	`define OR		5'b00001
	`define XNOR	5'b00110
	`define XOR		5'b01001
	`define ADD		5'b10001
	`define ADDAA	5'b10101
	`define ADDBB	5'b10011
	`define ADDAZ	5'b00101
	`define ADDBZ	5'b00011
	`define ADDZZ	5'b01111
	`define SUBAB	5'b10100
	`define SUBBA	5'b10010
	`define SUBAZ	5'b11101
	`define SUBBZ	5'b11011
	`define SUBZA	5'b01100
	`define SUBZB	5'b01010
	`define SUBZZ	5'b00000

//synthesis translate_off
	reg [39:0] op_ascii;
	always @* begin
		if (lc) begin
			case(opcode)
				`ADD:		op_ascii = "ADD  ";
				`ADDAA:		op_ascii = "ADDAA";
				`ADDBB:		op_ascii = "ADDBB";
				`ADDAZ:		op_ascii = "ADDAZ";
				`ADDBZ:		op_ascii = "ADDBZ";
				`ADDZZ:		op_ascii = "ADDZZ";
				`SUBAB:		op_ascii = "SUBAB";
				`SUBBA:		op_ascii = "SUBBA";
				`SUBAZ:		op_ascii = "SUBAZ";
				`SUBBZ:		op_ascii = "SUBBZ";
				`SUBZA:		op_ascii = "SUBZA";
				`SUBZB:		op_ascii = "SUBZB";
				`SUBZZ:		op_ascii = "SUBZZ";
				default:	op_ascii = " ??? ";
			endcase
		end else begin
			case(opcode)
				`AND:		op_ascii = "AND  ";
				`INVERTA:	op_ascii = "INVA ";
				`INVERTB:	op_ascii = "INVB ";
				`NOP:		op_ascii = "NOP  ";
				`MOVEA:		op_ascii = "MOVEA";
				`MOVEB:		op_ascii = "MOVEB";
				`NAND:		op_ascii = "NAND ";
				`NOR:		op_ascii = "NOR  ";
				`OR:		op_ascii = "OR   ";
				`XNOR:		op_ascii = "XNOR ";
				`XOR:		op_ascii = "XOR  ";
				default:	op_ascii = " ??? ";
			endcase
		end
	end
//synthesis translate_on

// first select the two adder inputs
	always @* begin
		case(opcode)
			`ADD:		ALUinA = operandA;
			`ADDAA:		ALUinA = operandA;
			`ADDBB:		ALUinA = operandB;
			`ADDAZ:		ALUinA = operandA;
			`ADDBZ:		ALUinA = operandB;
			`ADDZZ:		ALUinA = 8'h00;
			`SUBAB:		ALUinA = operandA;
			`SUBBA:		ALUinA = operandB;
			`SUBAZ:		ALUinA = operandA;
			`SUBBZ:		ALUinA = operandB;
			`SUBZA:		ALUinA = 8'h00;
			`SUBZB:		ALUinA = 8'h00;
			`SUBZZ:		ALUinA = 8'h00;
			default:ALUinA = 8'h00;
		endcase
	end

	always @* begin
		case(opcode)
			`ADD:		ALUinB = operandB;
			`ADDAA:		ALUinB = operandA;
			`ADDBB:		ALUinB = operandB;
			`ADDAZ:		ALUinB = 8'h00;
			`ADDBZ:		ALUinB = 8'h00;
			`ADDZZ:		ALUinB = 8'h00;
			`SUBAB:		ALUinB = ~operandB;
			`SUBBA:		ALUinB = ~operandA;
			`SUBAZ:		ALUinB = 8'hff;
			`SUBBZ:		ALUinB = 8'hff;
			`SUBZA:		ALUinB = ~operandA;
			`SUBZB:		ALUinB = ~operandB;
			`SUBZZ:		ALUinB = 8'hff;
			default:	ALUinB = 8'h00;
		endcase
	end

// set up carryin
	assign ci = bc | (multiprecisionCarry & mp);

// add
	assign tmpResult = ALUinA + ALUinB + ci;

// logic
	always @* begin
		case (opcode)
			`AND:		lResult = operandA & operandB;
			`INVERTA:	lResult = ~operandA;
			`INVERTB:	lResult = ~operandB;
			`MOVEA:		lResult = operandA;
			`MOVEB:		lResult = operandB;
			`NAND:		lResult = ~(operandA & operandB);
			`NOR:		lResult = ~(operandA | operandB);
			`NOP:		lResult = 8'h00;
			`OR:		lResult = operandA | operandB;
			`XNOR:		lResult = ~(operandA ^ operandB);
			`XOR:		lResult = operandA ^ operandB;
			default:	lResult = 8'h00;
		endcase
	end

// store carries
	always @ (posedge CLK) begin
		if (RST) begin
			multiprecisionCarry <= 1'b0;
		end else begin
			multiprecisionCarry <= tmpResult[8];
		end
	end

endmodule