/* mult.v
	Signed Multiplier with Double Accumulation
	Andrew W. Hill
	August 2006

	Two inputs are multiplied, then optionally, a third input
	and/or the high byte of the previous multiplication are
	added to the result.

	Signed multiplication is handled using a 9x9 multiplier
	The MSB acts as a sign bit.  If the high byte of the input
	is "1", and the sign enable is "1", then the number is
	negative, and the sign is propagated to the 9th bit.
	If the number is not negative or not signed, a "0" will be
	carried into the top bit of the multiplier, so it will be
	treated as positive.  The bottom 8 bits are mlo, the next
	eight are mhi, and the top two are trash used for sign
	extension.

	The Spartan4 appears to have a multiply-single-accumulate
	built in as a primitive... sweet!  Used 14slices in 5-6ns.
																*/

module mult (
	input  wire			CLK,
	input  wire			RST,
	input  wire [7:0]	operandA,
	input  wire [7:0]	operandB,
	input  wire			SA,			// operand A signed?
	input  wire			SB,			// operand B signed?
	input  wire [7:0]	operandC,	// accumulation operand
	input  wire			addmcEn,	// enables accumulation of third operand
	input  wire			addmhiEn,	// enables accumulation of mhi
	input  wire			on,			// is the PE on?

	output wire [7:0]	result,
	output reg  [7:0]	mhi
);

// signal instantiations
	wire [17:0] tmpResult;

// logic
	assign tmpResult = ({SA&operandA[7],operandA} * {SB&operandB[7],operandB}) + (operandC & {8{addmcEn}}) + (mhi & {8{addmhiEn}});
	assign result = tmpResult[7:0];

	always @ (posedge CLK) begin
		if (RST) begin
			mhi <= 8'h00;
		end else if (on) begin
			mhi <= tmpResult[15:8];
		end else begin
			mhi <= mhi;
		end
	end

//synthesis translate_off
	reg [55:0] op_ascii;
	always @* begin
		case({SA,SB})
			2'b00: op_ascii = "MULT   ";
			2'b01: op_ascii = "MULTSA ";
			2'b10: op_ascii = "MULTSB ";
			2'b11: op_ascii = "MULTSAB";
		endcase
	end
//synthesis translate_on

endmodule