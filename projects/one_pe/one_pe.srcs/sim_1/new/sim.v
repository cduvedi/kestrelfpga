`timescale 1ns / 1ps

module sim ();

// signal instantiations
	reg			CLK;
	reg			RST;
	reg			READ_L;
	reg [7:0]	led;


	top System (
		.CLK	(CLK),		// input  wire			CLK,
		.RST	(RST),		// input  wire			RST,
		.READ_L	(READ_L),	// input  wire			READ_L,
		.led	(led)		// output reg  [7:0]	led
	);

	always begin
		#5	CLK <= ~CLK;
	end

// testbench
	initial begin
		$display("Starting Simulation: sim");
			CLK <= 1;		// intialize clock
			RST <= 1;		// activate reset
			READ_L <= 1;	// initialize to read from left output
		#9	RST <= 0;		// release reset

		$monitor($stime, "    [MONITOR] out=%d", System.offLeft);

		#350 $display("Ending Simulation: SSRSim");
		$finish();
	end

endmodule: sim