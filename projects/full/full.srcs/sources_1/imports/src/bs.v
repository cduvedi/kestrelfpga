`timescale 1ns / 1ps

/**
* Bit Shifter (BS)
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Expected Functionality:
*	- The outputs of this module affect the next instruction's execution.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.8)
* 	  Comments include the corresponding nomenclature of the KASM manual
*	- Follows KASM manual for implementation except for the BS Flag MUX
*/

module bs (
	input  wire		 	CLK,
    input  wire			RST_L,
	input  wire  [3:0]	i_bit,		// bit_shift
	input  wire  [1:0]	flag_sel,	// (fSel, from i_fb)
	input  wire			i_force,	// frce
	input  wire			i_nop,		// controller nop
	input  wire			fb,			// flag
	input  wire  [7:0]	result,		// RESULT

	output reg   [7:0]	bs,			// bit shifter register
	output reg			mask,		// mask out
	output reg			bs_flag,	// BS Flag MUX
	output wire			pe_wor		// WOR
);

// signal instantiations
	reg  [7:0]	bs_next;
	reg			mask_next;
	wire		bs_nor = ~|bs;	// check if next or curr

	assign pe_wor = bs[7];

// i_bit list
	`define BSNOTMASK	4'b0001
	`define BSFLAGMASK	4'b0010
	`define BSCONDLEFT	4'b0011
	`define BSOR		4'b0100
	`define BSAND		4'b0101
	`define BSNOT		4'b0110
	`define BSSET		4'b0111
	`define BSCLEAR		4'b1000
	`define BSCLEARM	4'b1001
	`define BSPOPNOT	4'b1010
	`define BSPOP		4'b1011
	`define BSLATCH		4'b1100
	`define BSCONDLATCH	4'b1101
	`define BSPUSH		4'b1110
	`define BSCONDRIGHT	4'b1111

// bit shifter
	always @* begin
		case (i_bit)
			`BSAND:			bs_next = {bs[7] | fb, bs[6:0]};
			`BSCLEAR:		bs_next = 8'h00;
			`BSCLEARM:		bs_next = 8'h00;
			`BSCONDLATCH:	bs_next = mask ? bs : result;
			`BSCONDLEFT:	bs_next = mask ? bs : {bs[6:0], fb};
			`BSCONDRIGHT:	bs_next = mask ? bs : {fb, bs[7:1]};
			`BSFLAGMASK:	bs_next = bs;
			`BSLATCH:		bs_next = result;
			`BSNOT:			bs_next = {~bs[7], bs[6:0]};
			`BSNOTMASK:		bs_next = bs;
			`BSOR:			bs_next = {bs[7] & fb, bs[6:0]};
			`BSPOP:			bs_next = {bs[6:0],1'b0};
			`BSPOPNOT:		bs_next = {~bs[6], bs[5:0], 1'b0};
			`BSPUSH:		bs_next = {fb, bs[7:1]};
			`BSSET:			bs_next = {fb, bs[6:0]};
			default:		bs_next = bs;
		endcase
	end

	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			bs <= 8'h00;
		end else begin
			bs <= bs_next;
		end
	end

// mask flag
	always @* begin
		case (i_bit)
			`BSAND:			mask_next = bs_nor;
			`BSCLEAR:		mask_next = mask;
			`BSCLEARM:		mask_next = 1'b1;
			`BSCONDLATCH:	mask_next = mask;
			`BSCONDLEFT:	mask_next = mask;
			`BSCONDRIGHT:	mask_next = mask;
			`BSFLAGMASK:	mask_next = fb;
			`BSLATCH:		mask_next = bs_nor;
			`BSNOT:			mask_next = bs_nor;
			`BSNOTMASK:		mask_next = ~mask;
			`BSOR:			mask_next = bs_nor;
			`BSPOP:			mask_next = bs_nor;
			`BSPOPNOT:		mask_next = bs_nor;
			`BSPUSH:		mask_next = bs_nor;
			`BSSET:			mask_next = bs_nor;
			default:		mask_next = mask;
		endcase
	end

	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			mask <= 1'b0;
		end else begin
			mask <= (mask_next | i_force) & ~i_nop;
		end
	end

// BS Flag MUX
	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			bs_flag <= 1'b0;
		end else begin
			case (flag_sel[1:0])
				2'b00: bs_flag <= bs_nor;
				2'b01: bs_flag <= bs_next[0];
				2'b10: bs_flag <= bs_next[7];
				2'b11: bs_flag <= pe_wor;
			endcase
		end
	end

endmodule