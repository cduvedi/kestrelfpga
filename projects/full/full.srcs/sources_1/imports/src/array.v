`timescale 1ns / 1ps

/**
* Kestrel Array
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
*	- Controller instruction fields are labeled I_NAME
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module array (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire [52:0]	instruction,		// PE instruction
	input  wire			I_DATA_READ,		// io_w
	input  wire  [7:0]	dataFromControl,	// ssr data from offchip

	output wire  [7:0]	dataOutLeft,		// ssr data going left offchip
	output wire  [7:0]	dataOutRight,		// ssr data going right offchip
	output wire			offWOR				// wor signal going (right) offchip
);

	parameter	NPES = 4;
	parameter	NSSR = NPES+1;

	wire [52:0] instruction_fixed;
	assign instruction_fixed = {
		instruction[52:50],
		instruction[46],
		instruction[48:47],
		instruction[49],
		instruction[45:0]
	};

// PE instruction fields used by SSRs
	wire  [5:0]	i_dest = instruction_fixed[13:8];
	wire  [5:0]	i_opC  = instruction_fixed[19:14];
	wire  [5:0]	i_opA  = instruction_fixed[25:20];
	wire  [4:0]	i_func = instruction_fixed[50:46];

// Signal instantiations
	wire  [7:0]	opA	[0:NSSR-1];
	wire  [7:0]	opC	[0:NSSR-1];
	wire  [7:0]	result	[0:NPES-1];
	wire [NPES-1:0]	mask;
	wire [NPES-1:0]	pe_wor;

	genvar	i;

	assign	offWOR = |pe_wor;
	assign	dataOutLeft = result[0];
	assign	dataOutRight = result[NPES-1];

// Module instantiations
	generate
		// Shared Systolic Registers
		for (i = 0; i < NSSR; i = i + 1) begin : SSRs
			ssr s (
				.CLK			(CLK),										// input  wire			CLK,
				.RST_L			(RST_L),									// input  wire			RST_L,
				.i_opA			(i_opA[4:0]),								// input  wire [4:0]	i_opA,
				.i_opC			(i_opC[4:0]),								// input  wire [4:0]	i_opC,
				.i_dest			(i_dest),									// input  wire [5:0]	i_dest,
				.nop			(i_func == 5'b00101),						// input  wire			nop,
				.result_left	(i == 0 ? dataFromControl : result[i-1]),	// input  wire [7:0]	result_left,
				.result_right	(i == NPES ? dataFromControl : result[i]),	// input  wire [7:0]	result_right,
				.mask_left		(i == 0 ? I_DATA_READ : mask[i-1]),			// input  wire			mask_left,
				.mask_right		(i == NPES ? I_DATA_READ : mask[i]),		// input  wire			mask_right,
				.opA			(opA[i]),									// output wire [7:0]	opA,
				.opC			(opC[i])									// output wire [7:0]	opC
			);
		end

		// Processing Elements
		for (i = 0; i < NPES; i = i + 1) begin : PEs
			pe p (
				.CLK			(CLK),					// input  wire			CLK,
				.RST_L			(RST_L),				// input  wire			RST_L,
				.instruction	(instruction_fixed),	// input  wire [52:0]	instruction,
				.opAFromLeft	(opA[i]),				// input  wire  [7:0]	opAFromLeft,
				.opAFromRight	(opA[i+1]),				// input  wire  [7:0]	opAFromRight,
				.opCFromLeft	(opC[i]),				// input  wire  [7:0]	opCFromLeft,
				.opCFromRight	(opC[i+1]),				// input  wire  [7:0]	opCFromRight,
				.result			(result[i]),			// output wire  [7:0]	result,
				.pe_wor			(pe_wor[i]),			// output wire			pe_wor,
				.mask			(mask[i])				// output wire			mask
			);
		end
	endgenerate

endmodule