`timescale 1ns / 1ps

/**
* Comparator
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.6)
* 	  Comments include the corresponding nomenclature of the KASM manual
*	- Follows KASM manual for implementation except for the Comparator Flag MUX
*/

module cmp (
	input				CLK,
	input				RST_L,
	input  wire  [4:0]	i_fb,
	input  wire  [7:0]	opC,
	input  wire  [7:0]	alu_result,
	input  wire			mask,

	output reg			cmp_flag,
	output reg			eq_flag
);

	wire		reset_flag = i_fb[4:2] == 3'b000;
	wire		nonreset_flag = i_fb[4:2] == 3'b001;
	wire		cmp_en = reset_flag | (nonreset_flag & eq);

// Subtractor
	wire  [8:0] sopC = {opC[7], opC};
	wire  [8:0] salu_result = {alu_result[7], alu_result};
	wire  [8:0]	sub_result = salu_result - sopC;
	wire		sub_cout = sub_result[8];
	wire		sub_cts = opC[7] ^ alu_result[7] ^ sub_cout;	// p.33
	wire		sub_msb = sub_result[7];

// Equal Latch
	reg			eq;
	wire		eq_next = &sub_result;

	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) eq <= 1'b0;
		else if (mask & cmp_en) eq <= eq_next;
		else eq <= eq;
	end

// Min Latch
	reg			min;
	wire		min_next = cmp_flag;

	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) min <= 1'b0;
		else if (mask & (reset_flag | (nonreset_flag & eq))) min <= min_next;
		else min <= min;
	end

// Carry Flag MUX
	always @* begin
		case (i_fb[1:0])
			2'b00: cmp_flag = sub_cout;
			2'b01: cmp_flag = sub_cts;
			2'b10: cmp_flag = sub_msb;
			2'b11: cmp_flag = min;
		endcase
	end

// Equal Flag MUX
	always @* begin
		if (i_fb[0]) eq_flag = eq_next;
		else eq_flag = eq;
	end

endmodule: cmp