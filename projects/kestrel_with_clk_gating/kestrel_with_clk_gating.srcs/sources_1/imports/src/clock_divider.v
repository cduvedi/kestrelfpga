`timescale 1ns / 1ps

/**
* Kestrel SIMD Processor
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
*	- Controller instruction fields are labeled I_NAME
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module clock_divider (
	input wire clk_in,
	input wire RST_L,
	input wire en,
	output reg clk_div_2,
	output reg clk_div_4	
);

	reg [1:0] counter;

	always @(posedge clk_in)
	begin 
		if(en == 1'b1)	begin
			counter <= counter + 1;
		end
		else begin
			counter = 0;
		end
	end

	assign clk_div_2 = counter[0];
	assign clk_div_4 = counter[1];
endmodule
