`timescale 1ns / 1ps

/**
* Result Selector
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.7)
* 	  Comments include the corresponding nomenclature of the KASM manual
*	- Follows KASM manual for implementation except for the Comparator Flag MUX
*/

module resultSel (
	input  wire			flag,
	input  wire  [1:0]	i_rm,			// res_mx
	input  wire			i_lc,			// lc
	input  wire			i_ci,			// ci
	input  wire  [7:0]	mult_result,
	input  wire  [7:0]	alu_result,
	input  wire  [7:0]	opC,

	output reg   [7:0]	result
);

	always @* begin
		if (i_lc & i_ci) result = mult_result;
		else begin
			case (i_rm)
				2'b00, 2'b01: result = alu_result;
				2'b10: result = opC;
				2'b11: result = flag ? alu_result : opC;
			endcase
		end
	end

endmodule