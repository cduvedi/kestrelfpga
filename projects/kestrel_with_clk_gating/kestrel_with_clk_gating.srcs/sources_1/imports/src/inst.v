`timescale 1ns / 1ps

/**
* Instruction Memory
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly. (p.82)
* Comments include the corresponding nomenclature of the KASM manual.
*
* Implemented as a 96*256 memory (cannot have full size unfortunately)
*
*/

module inst (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire [15:0]	PC_OUT,

	output wire [95:0]	instruction
);

	inst_bram Mem (
		.clka				(CLK),
		.rsta				(~RST_L),
		.addra				(PC_OUT[7:0]),
		.douta				(instruction)
	);

endmodule: inst
