/* mem.v
	256x8b memory
	Nora Tarano
	July 2013
																*/

module sram (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire  [7:0]	i_imm,
	input  wire			i_wr,
	input  wire			i_rd,
	input  wire  [7:0]	opC,
	input  wire  [7:0]	result,
	input  wire			mask,

	output wire  [7:0]	mdr
);

	wire [7:0]	address = opC + i_imm;

	// IMPORTANT: this block must be write-first
	sram_bram Mem (
		.clka		(CLK),						// input		clka
		.rsta		(~RST_L),					// input		rsta
		.ena		(mask & (i_wr | i_rd)),		// input		ena
		.wea		(i_wr & ~i_rd),				// input  [0:0]	wea
		.addra		(address),					// input  [7:0]	addra
		.dina		(result),					// input  [7:0]	dina
		.douta		(mdr)						// output [7:0]	douta
	);

endmodule: sram