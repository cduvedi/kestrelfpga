//*************************************************
// 
// Clock gating implementation
// Aauthor - Chinmay Duvedi
//
//*************************************************

module clock_gate(
	input clock,
	input enable,
	input RST_L
	output clock_gated
);
	always @ (posedge clock, negedge RST_L) begin
		if(~RST_L) begin
			clock_gated = 1'b0;
		end else begin
			clock_gated = clock & enable;
		end
	end
endmodule
