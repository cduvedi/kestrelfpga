`timescale 1ns / 1ps

/**
* Kestrel SIMD Processor
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
*	- Controller instruction fields are labeled I_NAME
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module clock_switch_divider (
	input wire		clk_in,
	input wire 		clk_en,
	input wire 		RST_L,
	input wire [1:0]	clk_sel,
	output reg 		clk_out
)

// signal instantiations
	reg clk_div_2;		// The divided by 2 version of the clock
	reg clk_div_4;		// The divided by 4 version of the clock

// Instantiate the divider
	clk_divider clk_div (
		.clk_in		(clk_in),
		.RST_L		(RST_L),
		.en		(clk_en),
		.clk_div_2	(clk_div_2),
		.clk_div_4	(clk_div_4)
	);

	always @* begin
		if (clk_en = 1'b1) begin
			clk_out = 1'b0;
		end
		else begin
			case(clk_sel)
				2'b00: clk_out = 1'b0;
				2'b01: clk_out = clk_in;
				2'b10: clk_out = clk_div_2;
				2'b11: clk_out = clk_div_4;
				default: clk_out = 1'b0; 	// Ideally it should never come to this
			endcase
		end
	end
endmodule
