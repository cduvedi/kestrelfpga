`timescale 1ns / 1ps

/**
* Flag MUX
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.7)
* 	  Comments include the corresponding nomenclature of the KASM manual
*	- The mesh is not implemented so the corresponding encodings are ignored
*/

module flagMUX (
	input  wire  [4:2]	i_fb,
	input  wire			i_finv,
	input  wire			cmp_flag,
	input  wire			bs_flag,
	input  wire			alu_flag,
	input  wire			eq_flag,

	output wire			flag
);

// signal instantiations
	reg		flag_reg, flag_mux_result;

	assign	flag = flag_mux_result ^ i_finv;

// Flag MUX
	always @* begin
		case (i_fb[4:2])
			3'b000, 3'b001:	flag_mux_result = cmp_flag;
			3'b010:			flag_mux_result = bs_flag ;
			3'b011:			flag_mux_result = alu_flag;
			3'b100:			flag_mux_result = eq_flag ;
			default:		flag_mux_result = 1'b0;	// GND
		endcase
	end

endmodule