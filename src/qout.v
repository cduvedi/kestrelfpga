`timescale 1ns / 1ps

/**
* Output Queue
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly. (p.82,85)
*
*/

module qout (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire			I_CBS_LOAD,
	input  wire			I_CBS_SLEFT,
	input  wire			I_DATA_WRITE,
	input  wire			I_FIFO_OUT,
	input  wire  [7:0]	SCR_OUT,
	input  wire  [7:0]	BS_OUT,

	output wire   [7:0]	out,
	output wire			qout_int
);

	wire	qout_sel = I_CBS_LOAD | I_CBS_SLEFT;
	wire	write_en = I_DATA_WRITE | I_FIFO_OUT;
	wire	[7:0] data_out = qout_sel ? BS_OUT : SCR_OUT;
    wire    read_en = 1;                // FIXME: Need to get read from top
	
	assign	qout_int = full & write_en;	// TODO: or (empty and host read)
    queue qout(
        .CLK            (CLK),
        .RST_L          (RST_L),
        .queue_in       (data_out),
        .write_en       (write_en),
        .read_en        (read_en),
        .full           (full),
        .empty          (empty),
        .queue_out      (out)    
    );
endmodule: qout
	
	module queue (
    	input  wire			CLK,
    	input  wire			RST_L,
    	input  wire  [7:0]  queue_in,
    	input  wire         write_en,
        input  wire         read_en,
        output reg          full,
        output reg          empty,
    	output reg   [7:0]	queue_out
    );
    
    	reg     [3:0] counter;      // Counter to store the number of elements that have been pushed to the FIFO
    	reg     [3:0] rd_ptr;
    	reg     [3:0] wr_ptr;
    	reg     [7:0] buf_mem [15:0];
        
        // Empty / full logic
        always @(counter) begin
            full = (counter == 16);
            empty = (counter == 0);
        end
        
    	// Counter logic
    	always @(posedge CLK, negedge RST_L) begin
    	   if(~RST_L) begin
    	       counter <= 0;
    	   end else if( (!full && write_en) && ( !empty && read_en ) ) begin
    	       counter <= counter;
           end else if( (!full && write_en) && (counter < 15) )begin
               counter <= counter + 1;
           end else if( (!empty && read_en) && (counter > 0) ) begin
               counter <= counter - 1;
           end else begin
               counter <= counter;
           end
    	end
    
        // Output logic
        always @( posedge CLK or negedge RST_L) begin
           if(~RST_L)
              queue_out <= 0;
           else begin
              if( read_en && !empty )
                 queue_out <= buf_mem[rd_ptr];
        
              else
                 queue_out <= queue_out;
        
           end
        end
        
        // Input logic
        always @( posedge CLK, negedge RST_L) begin
            if(~RST_L) begin
                buf_mem[0] <= 0;
            end else begin
                if( write_en && !full ) begin
                    buf_mem[wr_ptr] <= queue_in;
                end else begin
                    buf_mem[wr_ptr] <= buf_mem[wr_ptr];
                end
            end
        end
        
        // Pointer logic
        always@(posedge CLK, negedge RST_L) begin
           if(~RST_L) begin
              wr_ptr <= 0;
              rd_ptr <= 0;
           end else begin
              if( !full && write_en ) begin
                  wr_ptr <= wr_ptr + 1;
              end else begin 
                  wr_ptr <= wr_ptr;
              end
              
              if( !empty && read_en ) begin
                 rd_ptr <= rd_ptr + 1;
              end else begin 
                 rd_ptr <= rd_ptr;
              end
           end
        
        end
    endmodule	
	
