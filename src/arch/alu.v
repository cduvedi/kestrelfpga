`timescale 1ns / 1ps

/**
* Arithmetic Logic Unit (ALU)
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.5)
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module alu (
	input  wire      	CLK,
	input  wire      	RST,
	input  wire [4:0]	i_func,
	input  wire      	i_lc,
	input  wire [7:0]	opA,
	input  wire [7:0]	opB,
	input  wire      	i_ci,			// ci
	input  wire      	i_mp,			// mp

	output wire [7:0]	alu_result,
	output wire			alu_flag
);

// signal instantiations
	reg			multiprecisionCarry;	// is a memory element
	reg  [7:0]	ALUinA;
	reg  [7:0]	ALUinB;
	reg  [7:0]	lResult;
	wire [8:0]	tmpResult;
	wire      	ci;
	wire		co;

// alu_result selector
	assign alu_result = i_lc ? tmpResult[7:0] : lResult;
	assign co     = i_lc ? tmpResult[8]   : 1'b0;			// dont spawn carry on logical ops

// opcode list
	`define AND		5'b00111
	`define INVERTA	5'b01010
	`define INVERTB	5'b01100
	`define MOVEA	5'b00101
	`define MOVEB	5'b00011
	`define NAND	5'b01000
	`define NOP		5'b00101
	`define NOR		5'b01110
	`define OR		5'b00001
	`define XNOR	5'b00110
	`define XOR		5'b01001
	`define ADD		5'b10001
	`define ADDAA	5'b10101
	`define ADDBB	5'b10011
	`define ADDAZ	5'b00101
	`define ADDBZ	5'b00011
	`define ADDZZ	5'b01111
	`define SUBAB	5'b10100
	`define SUBBA	5'b10010
	`define SUBAZ	5'b11101
	`define SUBBZ	5'b11011
	`define SUBZA	5'b01100
	`define SUBZB	5'b01010
	`define SUBZZ	5'b00000

//synthesis translate_off
	reg [39:0] op_ascii;
	always @* begin
		if (i_lc) begin
			case (i_func)
				`ADD:		op_ascii = "ADD  ";
				`ADDAA:		op_ascii = "ADDAA";
				`ADDBB:		op_ascii = "ADDBB";
				`ADDAZ:		op_ascii = "ADDAZ";
				`ADDBZ:		op_ascii = "ADDBZ";
				`ADDZZ:		op_ascii = "ADDZZ";
				`SUBAB:		op_ascii = "SUBAB";
				`SUBBA:		op_ascii = "SUBBA";
				`SUBAZ:		op_ascii = "SUBAZ";
				`SUBBZ:		op_ascii = "SUBBZ";
				`SUBZA:		op_ascii = "SUBZA";
				`SUBZB:		op_ascii = "SUBZB";
				`SUBZZ:		op_ascii = "SUBZZ";
				default:	op_ascii = " ??? ";
			endcase
		end else begin
			case (i_func)
				`AND:		op_ascii = "AND  ";
				`INVERTA:	op_ascii = "INVA ";
				`INVERTB:	op_ascii = "INVB ";
				`NOP:		op_ascii = "NOP  ";
				`MOVEA:		op_ascii = "MOVEA";
				`MOVEB:		op_ascii = "MOVEB";
				`NAND:		op_ascii = "NAND ";
				`NOR:		op_ascii = "NOR  ";
				`OR:		op_ascii = "OR   ";
				`XNOR:		op_ascii = "XNOR ";
				`XOR:		op_ascii = "XOR  ";
				default:	op_ascii = " ??? ";
			endcase
		end
	end
//synthesis translate_on

// first select the two adder inputs
	always @* begin
		case (i_func)
			`ADD:		ALUinA = opA;
			`ADDAA:		ALUinA = opA;
			`ADDBB:		ALUinA = opB;
			`ADDAZ:		ALUinA = opA;
			`ADDBZ:		ALUinA = opB;
			`ADDZZ:		ALUinA = 8'h00;
			`SUBAB:		ALUinA = opA;
			`SUBBA:		ALUinA = opB;
			`SUBAZ:		ALUinA = opA;
			`SUBBZ:		ALUinA = opB;
			`SUBZA:		ALUinA = 8'h00;
			`SUBZB:		ALUinA = 8'h00;
			`SUBZZ:		ALUinA = 8'h00;
			default:	ALUinA = 8'h00;
		endcase
	end

	always @* begin
		case (i_func)
			`ADD:		ALUinB = opB;
			`ADDAA:		ALUinB = opA;
			`ADDBB:		ALUinB = opB;
			`ADDAZ:		ALUinB = 8'h00;
			`ADDBZ:		ALUinB = 8'h00;
			`ADDZZ:		ALUinB = 8'h00;
			`SUBAB:		ALUinB = ~opB;
			`SUBBA:		ALUinB = ~opA;
			`SUBAZ:		ALUinB = 8'hff;
			`SUBBZ:		ALUinB = 8'hff;
			`SUBZA:		ALUinB = ~opA;
			`SUBZB:		ALUinB = ~opB;
			`SUBZZ:		ALUinB = 8'hff;
			default:	ALUinB = 8'h00;
		endcase
	end

// set up carryin
	assign ci = i_ci | (multiprecisionCarry & i_mp);

// add
	assign tmpResult = ALUinA + ALUinB + ci;

// logic
	always @* begin
		case (i_func)
			`AND:		lResult = opA & opB;
			`INVERTA:	lResult = ~opA;
			`INVERTB:	lResult = ~opB;
			`MOVEA:		lResult = opA;
			`MOVEB:		lResult = opB;
			`NAND:		lResult = ~(opA & opB);
			`NOR:		lResult = ~(opA | opB);
			`NOP:		lResult = 8'h00;
			`OR:		lResult = opA | opB;
			`XNOR:		lResult = ~(opA ^ opB);
			`XOR:		lResult = opA ^ opB;
			default:	lResult = 8'h00;
		endcase
	end

// store carries
	always @ (posedge CLK) begin
		if (RST) begin
			multiprecisionCarry <= 1'b0;
		end else begin
			multiprecisionCarry <= tmpResult[8];
		end
	end

endmodule