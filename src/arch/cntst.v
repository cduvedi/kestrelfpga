`timescale 1ns / 1ps

// uses 8bit pointers, so total size is limited to 256

module cntst (
	input  wire			CLK,
	input  wire			RST,
	input  wire [15:0]	datain,
	input  wire  [7:0]	eightin,
	input  wire			push,
	input  wire			dec,
	input  wire			lo,
	input  wire			hi,
	output wire			zero
);

	parameter size = 256;

	reg [15:0]	memory [size-1:0];
	reg  [7:0]	head;

	assign zero = (memory[head] == 16'h0000);

	// pointers
	always @ (posedge CLK) begin
		if (RST) begin
			head <= 8'h00;
		end else begin
			if (push) begin
				head <= head + 8'h01;
			end else begin
				if (dec) begin
					if (zero) begin
						head <= head - 8'h01;
					end else begin
						head <= head;
					end
				end else begin
					head <= head;
				end
			end
		end
	end

	// input
	always @ (posedge CLK) begin
		if (RST) begin
			memory[8'h00] <= 16'hffff;
			memory[8'hff] <= 16'hffff;
		end else begin
			if (push) begin
				memory[head+1] <= datain;
			end else begin
				memory[head+1] <= memory[head+8'h01];
			end

			if (dec) begin
		  		memory[head] <= memory[head] - 16'h0001;
			end else if (lo) begin
				memory[head] <= {{memory[head][15:8]},{eightin}};
			end else if (hi) begin
				memory[head] <= {{eightin},{memory[head][7:0]}};
			end else begin
			end

		end
	end

endmodule