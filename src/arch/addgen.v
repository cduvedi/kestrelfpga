/* addgen.v
	SRAM Address Generator
	Andrew W. Hill
	October 2006

	Simple adder to generate the final SRAM address.
	Kept in this file because it didn't belong anywhere else
		                                                   		*/

module addgen (
	input  wire [7:0] base,
	input  wire [7:0] offset,
	output wire [7:0] result
);

	assign result = base + offset;

endmodule