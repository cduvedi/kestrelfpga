`timescale 1ns / 1ps

/* fpka4.v
	Kestrel Level Top
	Andrew W. Hill
	October 2006

	All this does is take the length instruction and
	break it up, passing it to the PE and SSR.
	Mostly, this file keeps stuff straight in my head.

	I've tried to maintain the names used in the KASM Assembly
	Manual.

	I assume that register 5'b00000 is L0 and 5'b10000 is R0.
																						*/
module fpka4 (
	input  wire			CLK,
	input  wire			RST,
	input  wire			qtoarr,			// detailed description follows
	input  wire [51:0]	instructionIn,
	input  wire  [7:0]	dataInLeft,		// ssr data from offchip
	input  wire  [7:0]	dataInRight,		// ssr data from offchip

	output wire  [7:0]	offLeft,		// ssr data going left offchip
	output wire  [7:0]	offRight,		// ssr data going right offchip
	output wire			offWOR			// wor signal going (right) offchip
);

	/*********** EXPLANATION OF THE QUEUETOARR SIGNAL ************/
	/*  The qtoarr signal prAoperly belongs to the controller's   */
	/*  controller's instruction set, not to PE's.  Ordinarily,  */
	/*  the qtoarr would affect the inputs to the terminal chips */
	/*  on the board level.  However, in this simulation, there  */
	/*  is no real concept of "board-level".  The signal has     */
	/*  been added at this level out of necessity, however if    */
	/*  this design was ever implemented on a board, this signal */
	/*  and its associated logic would appear off-chip.          */
	/*************************************************************/


/*** These two lines correct for a wiring error on the board.	***
	*** With these lines, this project is compatible with the	***
	*** original kestrel board, and newkasm; however, it is at	***
	*** odds with the formal documentation.						***/
	wire [51:0] instruction;
	assign instruction = {
		instructionIn[51:50],
		instructionIn[46],
		instructionIn[48:47],
		instructionIn[49],
		instructionIn[45:0]
	};

	wire      	frce;
	wire [4:0]	alufunc;
	wire      	ci;
	wire      	mp;
	wire      	lc;
	wire      	fi;
	wire [4:0]	flagbus;
	wire [3:0]	bitshift;
	wire [1:0]	resmx;
	wire      	sramRead;
	wire      	sramWrite;
	wire [2:0]	opBsel;
	wire [5:0]	opAregister;
	wire [5:0]	opCregister;
	wire [5:0]	destregister;
	wire [7:0]	arrayimmediate;

// SSR result wires
	wire [7:0]	SSR0outA, SSR0outB;
	wire [7:0]	SSR1outA, SSR1outB;
	wire [7:0]	SSR2outA, SSR2outB;
	wire [7:0]	SSR3outA, SSR3outB;
	wire [7:0]	SSR4outA, SSR4outB;

// PE result wires
	wire [7:0]	PE0out;
	wire [7:0]	PE1out;
	wire [7:0]	PE2out;
	wire [7:0]	PE3out;

// WOR wires
	wire		PE0wor;
	wire		PE1wor;
	wire		PE2wor;
	wire		PE3wor;
	assign offWOR = PE3wor;

// Instruction fields
	assign frce			= instruction[51];
	assign alufunc		= instruction[50:46];
	assign ci			= instruction[45];
	assign mp			= instruction[44];
	assign lc			= instruction[43];
	assign fi			= instruction[42];
	assign flagbus		= instruction[41:37];
	assign bitshift		= instruction[36:33];
	assign resmx		= instruction[32:31];
	assign sramRead		= instruction[30];
	assign sramWrite	= instruction[29];
	assign opBsel		= instruction[28:26];
	assign opAregister	= instruction[25:20];
	assign opCregister	= instruction[19:14];
	assign destregister	= instruction[13:8];
	assign arrayimmediate = instruction[7:0];

// Offchip outputs
	assign offLeft  = PE0out;
	assign offRight = PE1out;	// NOTE: ??? What is this

// WEs
	wire	PE0we;
	wire	PE1we;
	wire	PE2we;
	wire	PE3we;

// PE and SSR instantiations

	// LEFT EDGE OF "CHIP" STARTS HERE

	ssr SSR0 (
		.CLK				(CLK),
		.RST				(RST),
		.whichRegA			(opAregister[4:0]),
		.whichRegB			(opCregister[4:0]),
		.whichRegIn			(destregister[4:0]),
		.fromLeft			(destregister[5]),
		.dataInfromLeft		(dataInLeft),
		.dataInfromRight	(PE0out),
		.weL				(qtoarr),
		.weR				(PE0we),
		.dataOutA			(SSR0outA),
		.dataOutB			(SSR0outB)
	);


	pe PE0 (
		.CLK				(CLK),
		.RST				(RST),
		.frce				(frce),
		.alufunc			(alufunc),
		.ci					(ci),
		.mp					(mp),
		.lc					(lc),
		.fi					(fi),
		.flagbus			(flagbus),
		.bitshift			(bitshift),
		.resmx				(resmx),
		.sramRead			(sramRead),
		.sramWrite			(sramWrite),
		.opBsel				(opBsel),
		.fromRightA			(opAregister[5]),
		.fromRightC			(opCregister[5]),
		.arrayimmediate		(arrayimmediate),
		.inLA				(SSR0outA),
		.inLC				(SSR0outB),
		.inRA				(SSR1outA),
		.inRC				(SSR1outB),
		.worin				(1'b0),
		.worout				(PE0wor),
		.out				(PE0out),
		.on  				(PE0we)
	);

	ssr SSR1 (
		.CLK				(CLK),
		.RST				(RST),
		.whichRegA			(opAregister[4:0]),
		.whichRegB			(opCregister[4:0]),
		.whichRegIn			(destregister[4:0]),
		.fromLeft			(destregister[5]),
		.dataInfromLeft		(PE0out),
		.dataInfromRight	(PE1out),
		.weL				(PE0we),
		.weR				(PE1we),
		.dataOutA			(SSR1outA),
		.dataOutB			(SSR1outB)
	);


	pe PE1 (
		.CLK				(CLK),
		.RST				(RST),
		.frce				(frce),
		.alufunc			(alufunc),
		.ci					(ci),
		.mp					(mp),
		.lc					(lc),
		.fi					(fi),
		.flagbus			(flagbus),
		.bitshift			(bitshift),
		.resmx				(resmx),
		.sramRead			(sramRead),
		.sramWrite			(sramWrite),
		.opBsel				(opBsel),
		.fromRightA			(opAregister[5]),
		.fromRightC			(opCregister[5]),
		.arrayimmediate		(arrayimmediate),
		.inLA				(SSR1outA),
		.inLC				(SSR1outB),
		.inRA				(SSR2outA),
		.inRC				(SSR2outB),
		.worin				(PE0wor),
		.worout				(PE1wor),
		.out				(PE1out),
		.on					(PE1we)
	);

	ssr SSR2 (
		.CLK				(CLK),
		.RST				(RST),
		.whichRegA			(opAregister[4:0]),
		.whichRegB			(opCregister[4:0]),
		.whichRegIn			(destregister[4:0]),
		.fromLeft			(destregister[5]),
		.dataInfromLeft		(PE1out),
		.dataInfromRight	(PE2out),
		.weL				(PE1we),
		.weR				(PE2we),
		.dataOutA			(SSR2outA),
		.dataOutB			(SSR2outB)
	);


	pe PE2 (
		.CLK				(CLK),
		.RST				(RST),
		.frce				(frce),
		.alufunc			(alufunc),
		.ci					(ci),
		.mp					(mp),
		.lc					(lc),
		.fi					(fi),
		.flagbus			(flagbus),
		.bitshift			(bitshift),
		.resmx				(resmx),
		.sramRead			(sramRead),
		.sramWrite			(sramWrite),
		.opBsel				(opBsel),
		.fromRightA			(opAregister[5]),
		.fromRightC			(opCregister[5]),
		.arrayimmediate		(arrayimmediate),
		.inLA				(SSR2outA),
		.inLC				(SSR2outB),
		.inRA				(SSR3outA),
		.inRC				(SSR3outB),
		.worin				(PE1wor),
		.worout				(PE2wor),
		.out				(PE2out),
		.on 				(PE2we)
	);

	ssr SSR3 (
		.CLK				(CLK),
		.RST				(RST),
		.whichRegA			(opAregister[4:0]),
		.whichRegB			(opCregister[4:0]),
		.whichRegIn			(destregister[4:0]),
		.fromLeft			(destregister[5]),
		.dataInfromLeft		(PE2out),
		.dataInfromRight	(PE3out),
		.weL				(PE2we),
		.weR				(PE3we),
		.dataOutA			(SSR3outA),
		.dataOutB			(SSR3outB)
	);


	pe PE3 (
		.CLK				(CLK),
		.RST				(RST),
		.frce				(frce),
		.alufunc			(alufunc),
		.ci					(ci),
		.mp					(mp),
		.lc					(lc),
		.fi					(fi),
		.flagbus			(flagbus),
		.bitshift			(bitshift),
		.resmx				(resmx),
		.sramRead			(sramRead),
		.sramWrite			(sramWrite),
		.opBsel				(opBsel),
		.fromRightA			(opAregister[5]),
		.fromRightC			(opCregister[5]),
		.arrayimmediate		(arrayimmediate),
		.inLA				(SSR3outA),
		.inLC				(SSR3outB),
		.inRA				(SSR4outA),
		.inRC				(SSR4outB),
		.worin				(PE3wor),
		.worout				(PE3wor),
		.out				(PE3out),
		.on					(PE3we)
	);


	ssr SSR4 (
		.CLK				(CLK),
		.RST				(RST),
		.whichRegA			(opAregister[4:0]),
		.whichRegB			(opCregister[4:0]),
		.whichRegIn			(destregister[4:0]),
		.fromLeft			(destregister[5]),
		.dataInfromLeft		(PE3out),
		.dataInfromRight	(dataInRight),
		.weL				(PE3we),
		.weR				(qtoarr),
		.dataOutA			(SSR4outA),
		.dataOutB			(SSR4outB)
	);

// RIGHT EDGE OF "CHIP" ENDS HERE

endmodule