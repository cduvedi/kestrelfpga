`timescale 1ns / 1ps

module issuer (
	input  wire			CLK,
	input  wire			RST,
	output wire [51:0]	instruction,
	output wire			qtoarr,
	output wire  [7:0]	fromLeft
);

	wire [63:0] memory [98:0];
	reg   [7:0] pc;
	reg  [63:0] current;
	always @ (posedge CLK) begin : dont_optimize_me
		if (RST) begin
			pc <= 8'h00;
			current = memory[0];
		end else begin
			pc <= pc + 1'b1;
			current = memory[pc];
		end
	end

	assign instruction = current[63:12];
	// burn 3 bits to make reading instructions easier
	assign qtoarr = current[8];
	assign fromLeft = current[7:0];

	assign memory[0]  = 64'hb010000000000000;		//		nop
	assign memory[1]  = 64'hbc90000000000000;		//		addzz L0
	assign memory[2]  = 64'he09001c002201000;		//		add R2,L0,#1
	assign memory[3]  = 64'he09001e202201000;		//		add R2,R2,#1
	assign memory[4]  = 64'hd29001e200103000;		//		sub L1,R2,#3
	assign memory[5]  = 64'hbe90000000000000;		//		addzz c1 L0
	assign memory[6]  = 64'h8710002202000000;		//		mult R0,R2,L0
	assign memory[7]  = 64'ha89001c000005000;		//		addxz L0,#4
	assign memory[8]  = 64'ha89001c002204000;		//		addxz R2,#5
	assign memory[9]  = 64'hb810000088100000;		//		and L1,L0,R2
	assign memory[10] = 64'ha89001c000108000;		//		addxz L1,#8
	assign memory[11] = 64'ha88019c004005000;		//		addxz L0,#5 maxc L1
	assign memory[12] = 64'ha8c019c000103000;		//		addxz L1,#3 minc L0
	assign memory[13] = 64'hb010000000000000;		//		nop
// kex01.kasm
	assign memory[14] = 64'hb010000000000000;		//		nop
	assign memory[15] = 64'hbc90000000000000;		//		addzz L0
	assign memory[16] = 64'he09001c000003000;		//		add L0,L0,#3
	assign memory[17] = 64'hb010000000000000;		//		nop
// SORT 1 (all PEs on)
	assign memory[18] = 64'hb010000000000001;		//		nop
	assign memory[19] = 64'h8090000000000001;		//		subzz L0 b1
	assign memory[20] = 64'h8090000000100001;		//		subzz L1 b1
	assign memory[21] = 64'h8090000002100001;		//		subzz R1 b1
	assign memory[22] = 64'hb000180004200001;		//		move L2,L0 maxc L1
	assign memory[23] = 64'hb040180004100001;		//		move L1,L0 minc L1
	assign memory[24] = 64'hb010000202000101;		//		move R0,L2 qtoarr
	assign memory[25] = 64'hb000180004200005;		//		move L2,L0 maxc L1
	assign memory[26] = 64'hb040180004100005;		//		move L1,L0 minc L1
	assign memory[27] = 64'hb010000202000105;		//		move R0,L2 qtoarr
	assign memory[28] = 64'hb000180004200002;		//		move L2,L0 maxc L1
	assign memory[29] = 64'hb040180004100002;		//		move L1,L0 minc L1
	assign memory[30] = 64'hb010000202000102;		//		move R0,L2 qtoarr
	assign memory[31] = 64'hb000180004200007;		//		move L2,L0 maxc L1
	assign memory[32] = 64'hb040180004100007;		//		move L1,L0 minc L1
	assign memory[33] = 64'hb010000202000107;		//		move R0,L2 qtoarr
	assign memory[34] = 64'hb000180004200000;		//		move L2,L0 maxc L1
	assign memory[35] = 64'hb040180004100000;		//		move L1,L0 minc L1
	assign memory[36] = 64'hb010000202000000;		//		move R0,L2
	assign memory[37] = 64'hb000180004200000;		//		move L2,L0 maxc L1
	assign memory[38] = 64'hb040180004100000;		//		move L1,L0 minc L1
	assign memory[39] = 64'hb010000202000000;		//		move R0,L2
	assign memory[40] = 64'hb000180004200000;		//		move L2,L0 maxc L1
	assign memory[41] = 64'hb040180004100000;		//		move L1,L0 minc L1
	assign memory[42] = 64'hb010000202000000;		//		move R0,L2
	assign memory[43] = 64'hb000180004200000;		//		move L2,L0 maxc L1
	assign memory[44] = 64'hb040180004100000;		//		move L1,L0 minc L1
	assign memory[45] = 64'hb010000202000000;		//		move R0,L2
	assign memory[46] = 64'hb040182086100000;		//		move R1,R0 minc R1
	assign memory[47] = 64'hb010000000000000;		//		nop
// SORT 2 (conditional)
// Clear 0,1,2
	assign memory[48] = 64'hbc90000000000000;		//		addzz L0		// PE#
	assign memory[49] = 64'hbc90000000100000;		//		addzz L1		// value
	assign memory[50] = 64'hbc90000000200000;		//		addzz L2		// temp
	assign memory[51] = 64'hbe90000000300000;		//		addzz L3 c1	// set L3 to 1
// Number PEs
	assign memory[52] = 64'hb010000000000000;		//		nop
	assign memory[53] = 64'hbe90000000000000;		//		addzz c1 L0
	assign memory[54] = 64'hb290000002000000;		//		addxz c1 R0,L0
	assign memory[55] = 64'hb290000002000000;		//		addxz c1 R0,L0
// Input data
	assign memory[56] = 64'hb010000102100101;		//		move R1,L1 qtoarr
	assign memory[57] = 64'hb010000102100105;		//		move R1,L1 qtoarr
	assign memory[58] = 64'hb010000102100102;		//		move R1,L1 qtoarr
	assign memory[59] = 64'hb010000102100107;		//		move R1,L1 qtoarr
	assign memory[60] = 64'hb010000000000004;		//		nop qtoarr

// begincond
	assign memory[61] = 64'h3861c1c00c401000;		//		and L4,L0,#1 equalc L3 bspush
	assign memory[62] = 64'h3081c00184100000;		//			addxz L1,L1 ltc R1 bspush
	assign memory[63] = 64'h3010002100200000;		//				move L2,R1
	assign memory[64] = 64'h3010000102200000;		//				move R2,L1
	assign memory[65] = 64'h3010c00000000000;		//			bsnot
	assign memory[66] = 64'h3010000100200000;		//				move L2,L1
	assign memory[67] = 64'h3010002102200000;		//				move R2,R1
	assign memory[68] = 64'h3011600000000000;		//			bspop
	assign memory[69] = 64'h3011600000000000;		//		bspop
	assign memory[70] = 64'h3290000000000000;		//		addxz L0,L0 c1
	assign memory[71] = 64'h3010000200100000;		//		move L1,L2
	assign memory[72] = 64'h3010002202100000;		//		move R1,R2

	assign memory[73] = 64'h3861c1c00c401000;		//		and L4,L0,#1 equalc L3 bspush
	assign memory[74] = 64'h3081c00184100000;		//			addxz L1,L1 ltc R1 bspush
	assign memory[75] = 64'h3010002100200000;		//				move L2,R1
	assign memory[76] = 64'h3010000102200000;		//				move R2,L1
	assign memory[77] = 64'h3010c00000000000;		//			bsnot
	assign memory[78] = 64'h3010000100200000;		//				move L2,L1
	assign memory[79] = 64'h3010002102200000;		//				move R2,R1
	assign memory[80] = 64'h3011600000000000;		//			bspop
	assign memory[81] = 64'h3011600000000000;		//		bspop
	assign memory[82] = 64'h3290000000000000;		//		addxz L0,L0 c1
	assign memory[83] = 64'h3010000200100000;		//		move L1,L2
	assign memory[84] = 64'h3010002202100000;		//		move R1,R2

	assign memory[85] = 64'h3861c1c00c401000;		//		and L4,L0,#1 equalc L3 bspush
	assign memory[86] = 64'h3081c00184100000;		//			addxz L1,L1 ltc R1 bspush
	assign memory[87] = 64'h3010002100200000;		//				move L2,R1
	assign memory[88] = 64'h3010000102200000;		//				move R2,L1
	assign memory[89] = 64'h3010c00000000000;		//			bsnot
	assign memory[90] = 64'h3010000100200000;		//				move L2,L1
	assign memory[91] = 64'h3010002102200000;		//				move R2,R1
	assign memory[92] = 64'h3011600000000000;		//			bspop
	assign memory[93] = 64'h3011600000000000;		//		bspop
	assign memory[94] = 64'h3290000000000000;		//		addxz L0,L0 c1
	assign memory[95] = 64'h3010000200100000;		//		move L1,L2
	assign memory[96] = 64'h3010002202100000;		//		move R1,R2
//endcond

	assign memory[97] = 64'hb010000000000000;		//		nop
	assign memory[98] = 64'hb010000000000000;		//		nop

endmodule