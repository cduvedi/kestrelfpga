/* cmp.v
	Comparator
	Andrew W. Hill
	August 2006

	Supports minc,maxc,sminc,smaxc.  Does not support mod256
	operations.
		                                                   		*/

module cmp (
	input				CLK,
	input				RST,
	input  wire [7:0]	alu_result,
	input  wire [7:0]	opC,
	input  wire [1:0]	i_rm,

	input  wire			max,		// 0: return max, 1: return min
	input  wire			sign,		// 0: inputs are unsigned, 1: inputs are signed
	input  wire			on,

	output reg  [7:0]	result,		// one out
	output wire			eq,
	output wire			min,
	output reg			eqLatch,
	output reg			minLatch
);

// signal instantiations
	wire [8:0] salu_result, sopC;		// extra bit used for signed comparisons
	reg  [8:0] tmpresult;
	wire [8:0] cmptmp;

// logic
	assign salu_result = {alu_result[7] & sign, alu_result};
	assign sopC = {opC[7] & sign, opC};

	always @* begin
		if (min) begin
			tmpresult = max ? salu_result : sopC;  // B>A
		end else begin
			tmpresult = max ? sopC : salu_result;  // A>B
		end
	end

	always @* begin
		case (i_rm)
			2'b10:   result = opC;
			2'b11:   result = tmpresult[7:0];
			default: result = alu_result;
		endcase
	end

	assign cmptmp = salu_result - sopC;
	assign eq = &cmptmp;
	assign min = cmptmp[8];	// 0: A>B, 1: B>A

	always @ (posedge CLK) begin
		if (RST) begin
			eqLatch <= 1'b0;
			minLatch <= 1'b0;
		end else if (on) begin
			eqLatch <= eq;
			minLatch <= min;
		end else begin
			eqLatch <= eqLatch;
			minLatch <= minLatch;
		end
	end

//synthesis translate_off
	reg [31:0] op_ascii;
	always @* begin
		case({i_rm,max,sign})
			4'b1000: op_ascii = "OP C";
			4'b1001: op_ascii = "OP C";
			4'b1010: op_ascii = "OP C";
			4'b1011: op_ascii = "OP C";
			4'b1110: op_ascii = "MIN ";
			4'b1111: op_ascii = "SMIN";
			4'b1100: op_ascii = "MAX ";
			4'b1101: op_ascii = "SMAX";
			default: op_ascii = "ALU ";
		endcase
	end
//synthesis translate_on

endmodule