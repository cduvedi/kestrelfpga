/* imem.v
	Kestrel Instruction Memory
	Andrew W. Hill
	March 2007

	Holds the 96bit wide instructions.
	In all likelihood this will be on a different chip.

																						*/

module imem (
	input  wire			CLK,
	input  wire			RST,
	input  wire [15:0]	pc,
	output wire [95:0]	instruction
);

	parameter size = 128;

	wire [95:0]	memory [size-1:0];
	reg  [95:0]	current;


	always @ (posedge CLK) begin
		if (RST) begin
			current = memory[0];
		end else begin
			current = memory[pc];
		end
	end

	assign instruction = current;

	// kex01.kasm
/*		assign memory[0]  = 96'hb01000000000020000218001;
		assign memory[1]  = 96'hbc9000000000020000018000;	// addzz   L0
		assign memory[2]  = 96'he09001c00000320000018000;	// add     L0,L0,#3
		assign memory[3]  = 96'hb01000000000020000118003;
*/	// kex02.kasm
/*		assign memory[0]  = 96'hb01000000000020000218001;
		assign memory[1]  = 96'hbe9000000000020000018000;	// addzz c1 L0
		assign memory[2]  = 96'hb29000000200020000018000;	// addxz c1 R0, L0
		assign memory[3]  = 96'hb29000000200020000018000;	// addxz c1 R0, L0
		assign memory[4]  = 96'hb29000000200020000018000;	// addxz c1 R0, L0
		assign memory[5]  = 96'hb01000000000020000118003;
*/	// kex03.kasm
/*		assign memory[0] = 96'hb01000000000020000218001;
		assign memory[1] = 96'hbe9000000000020000018000;	// addzz c1 L0
		assign memory[2] = 96'hb01000000000020000418010;	// beginloop 3
		assign memory[3] = 96'hb29000000200020000018000;	// 	addxz c1 R0, L0
		assign memory[4] = 96'hb01000000000020000618060;	// endloop
		assign memory[5] = 96'hb01000000000020000118003;
*/	// test.kasm
/*		assign memory[0]  = 96'hb01000000000020000218001;
		assign memory[1]  = 96'h3861c1c00c40120000018000;	// and L4,L0,#1 equalc L3 bspush
		assign memory[2]  = 96'h3081c0018410020000018000;	// 	addxz L1,L1 ltc R1 bspush
		assign memory[3]  = 96'h301000010020020000018000;	//			move L2,L1
		assign memory[4]  = 96'h301000210220020000018000;	//			move R2,R1
		assign memory[5]  = 96'h3010c0000000020000018000;	//		bsnot
		assign memory[6]  = 96'h301000210020020000018000;	//			move L2,R1
		assign memory[7]  = 96'h301000010220020000018000;	//			move R2,L1
		assign memory[8]  = 96'h301160000000020000018000;	//		bspop
		assign memory[9] = 96'h301160000000020000018000;	//	bspop
		assign memory[10] = 96'h329000000000020000018000;	//	addxz L0,L0 c1
		assign memory[11] = 96'h301000020010020000018000;	//	move L1,L2
		assign memory[12] = 96'h301000000000020001c18001;	//	jump NEXT
		assign memory[13] = 96'h301000220220020000018000;	//	move R2,R2
		assign memory[14] = 96'h301000220220020000018000;	//	NEXT: move R2,R2
		assign memory[15] = 96'hb01000000000020000118003;
*/	// oddeven_10.kasm
		assign memory[0]  = 96'hb010000000000_20000218001;  //
		assign memory[1]  = 96'hbe90000001f00_20000018000;  // addzz c1 L31
		assign memory[2]  = 96'hbe90000000100_20000018000;  // addzz c1 L1
		assign memory[3]  = 96'hbc90000000000_20000018000;  // addzz L0
		assign memory[4]  = 96'hb010000000000_20000618010;  // beginloop 4
		assign memory[5]  = 96'hb290001f03f00_20000018000;  // 	addxz c1 R31, L31
		assign memory[6]  = 96'hb010000000000_20000a18060;  // endloop
		assign memory[7]  = 96'hb010000000000_20000618010;  // beginloop 4
		assign memory[8]  = 96'hb010002200200_20000038000;  // 	move L2,R2 qtoarr
		assign memory[9]  = 96'hb010000000000_20001018060;  // endloop
		assign memory[10] = 96'h3010000000000_20000618010;  // beginloop 4
		assign memory[11] = 96'h3861c1df04101_20000018000;  // 	and L1,L31,#1 equalc L1 bspush
		assign memory[12] = 96'h3010001f00000_20000018000;  // 		move L0,L31
		assign memory[13] = 96'h3010001f02000_20000018000;  // 		move R0,L31
		assign memory[14] = 96'h3081c00288200_20000018000;  // 		addxz L2,L2 ltc R2 bspush
		assign memory[15] = 96'h3010000200500_20000018000;  // 			move L5,L2
		assign memory[16] = 96'h3010002202500_20000018000;  // 			move R5,R2
		assign memory[17] = 96'h3010c00000000_20000018000;  // 		bsnot
		assign memory[18] = 96'h3010002200500_20000018000;  // 			move L5,R2
		assign memory[19] = 96'h3010000202500_20000018000;  //			move R5,L2
		assign memory[20] = 96'h3011600000000_20000018000;  // 		bspop
		assign memory[21] = 96'h3011600000000_20000018000;  // 	bspop
		assign memory[22] = 96'h3290001f01f00_20000018000;  // 	addxz c1 L31, L31
		assign memory[23] = 96'h3010000500200_20000018000;  // 	move L2,L5
		assign memory[24] = 96'h3010002502200_20000018000;  // 	move R2,R5
		assign memory[25] = 96'h3010000000000_20001618060;  // endloop
		assign memory[26] = 96'hb010000000000_20000618010;  // beginloop 4
		assign memory[27] = 96'hb010002500500_2000005c000;  // 	move L5,R5 arrtoq
		assign memory[28] = 96'hb010000000000_20003418060;  // endloop
		assign memory[29] = 96'hb010000000000_20000118003;  //

endmodule