`timescale 1ns / 1ps

// uses 16bit pointers, so total size is limited to 64k

module stack (
	input  wire			CLK,
	input  wire			RST,
	input  wire [15:0]	datain,
	input  wire			we,
	input  wire			oe,
	output reg  [15:0]	dataout
);

// signal instantiations
	parameter size = 4096;

	reg [15:0]	memory [size-1:0];

	reg [15:0]	head;

// pointers logic
	always @ (posedge CLK) begin
		if (RST) begin
			head <= 16'h0000;
		end else begin
			if (oe) begin
				head <= head - 16'h0001;
			end else begin
				if (we) begin
					head <= head + 16'h0001;
				end else begin
					head <= head;
				end
			end
		end
	end

// output register logic
	always @ (posedge CLK) begin
		if (RST) begin
			dataout <= 16'h0000;
		end else begin
			if (oe) begin
				dataout <= memory[head-16'h0001];
			end else begin
				dataout <= dataout;
			end
		end
	end

// input logic
	always @ (posedge CLK) begin
		if (we) begin
			memory[head] <= datain;
		end
	end


endmodule