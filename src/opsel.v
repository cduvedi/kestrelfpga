`timescale 1ns / 1ps

/**
* Operand Selector
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible (section 2.4.2)
* 	  Comments include the corresponding nomenclature of the KASM manual
*/


module opsel (
	input  wire [7:0]	opAFromLeft,
	input  wire [7:0]	opCFromLeft,
	input  wire [7:0]	opAFromRight,
	input  wire [7:0]	opCFromRight,
	input  wire			fromRightA,		// 0:left, 1:right
	input  wire			fromRightC,		// 0:left, 1:right
	input  wire [2:0]	i_opB,
	input  wire [7:0]	mdr,
	input  wire [7:0]	mhi,
	input  wire [7:0]	bs,
	input  wire [7:0]	i_imm,

	output wire [7:0]	opA,
	output reg  [7:0]	opB,
	output wire [7:0]	opC
);

// Operands A and C Selection
// Selects whether to use the right or left inputs.
	assign opA = fromRightA ? opAFromRight : opAFromLeft;
	assign opC = fromRightC ? opCFromRight : opCFromLeft;

// Operand B Selection
// The top two bits indicate the source, the bottom bit indicates sign extension
// This is not true for 110 and 111, which are BS and Imm, respectively.  I had
// this explicitly masked at one point, but this seems to optimize better.
	always @* begin
		case (i_opB)
			3'b000: opB = opC;			// opbreg
			3'b001: opB = {8{opC[7]}};	// opbsreg
			3'b010: opB = mdr;			// mdr
			3'b011: opB = {8{mdr[7]}};	// smdr
			3'b100: opB = mhi;			// mhi
			3'b101: opB = {8{mhi[7]}};	// smhi
			3'b110: opB = bs;			// bs
			3'b111: opB = i_imm;		// imm
		endcase
	end

endmodule