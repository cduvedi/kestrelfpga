`timescale 1ns / 1ps

// TODO: check where _c may be needed, see page 88 of thesis

module control (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire  [7:0]	dataInLeft,
	input  wire  [7:0]	dataInRight,
	input  wire			inWOR,
    input  wire  [7:0]  DATA_IN,
    output wire  [7:0]  DATA_OUT,
	output wire [52:0]	PEInstruction,
	output wire			I_DATA_READ,			// indicate whether or not data will be written to the array
	output wire  [7:0]	dataToArray
);

// signal declarations
	wire [95:0]	instructionFromMemory;
	reg  [43:0]	controlInstructionFromMemoryB;
	reg  [43:0]	controlInstructionFromMemoryC;
    //wire [7:0]  DATA_OUT;
    reg   [7:0] data_scr;
	wire  [7:0]	dataFromArray;
	wire		loopzero;
	wire		qout_int;
	wire		qin_int;

	wire [15:0]	PC_OUT;
	wire  [7:0]	SCR_OUT;
	wire  [7:0]	BS_OUT;
	wire		W_OR;
	wire  [7:0] Q_IN;

	// instruction fields
	wire  [1:0] I_PC_SEL		= controlInstructionFromMemoryA[1:0];		// pc_sel
	wire		I_PUSH_PC		= controlInstructionFromMemoryA[2];			// pc_pu
	wire		I_POP_PC		= controlInstructionFromMemoryA[3];			// pc_po
	wire		I_PUSH_CNT		= controlInstructionFromMemoryA[4];			// cnt_ps
	wire		I_DEC_CNT		= controlInstructionFromMemoryA[5];			// cnt_d
	wire 		I_BR_0			= controlInstructionFromMemoryA[6];			// br_c
	wire		I_BR_W_OR_c		= controlInstructionFromMemoryC[7];			// br_w
	wire  [1:0]	I_CNT_LOAD_c	= controlInstructionFromMemoryC[9:8];		// imm_sel
	wire		I_CBS_LOAD_c	= controlInstructionFromMemoryC[10];		// cbs_ld
	wire		I_CBS_SLEFT_c	= controlInstructionFromMemoryC[11];		// cbs_sh
	wire  [1:0]	I_SCR_SEL_c		= controlInstructionFromMemoryC[13:12];		// scratch_mx
	wire		I_SCR_STORE_c	= controlInstructionFromMemoryC[14];		// scratch_w
	wire  [1:0]	I_IN_DATA_SEL	= controlInstructionFromMemoryA[16:15];		// dsel
	assign		I_DATA_READ		= controlInstructionFromMemoryA[17];		// io_w (qtoarr)
	wire		I_DATA_WRITE_c	= controlInstructionFromMemoryC[18];		// io_r (arrtoq)
	wire		I_FIFO_OUT		= controlInstructionFromMemoryC[19];		// f_ot
	wire [15:0]	I_IMM			= controlInstructionFromMemoryA[36:21];		// c_imm
	wire		D_Pop_Cnt		= controlInstructionFromMemoryA[37];
	wire		BOARD_IMM_MUX	= controlInstructionFromMemoryA[41];
	wire  [7:0]	I_PE_IMM		= instructionFromMemory[51:44];
	wire  [5:0]	I_PE_DEST		= PEInstructionFromMemory[5:0];

	wire [43:0]	controlInstructionFromMemoryA = instructionFromMemory[43:0];
	wire [43:0]	PEInstructionFromMemory = instructionFromMemory[95:52];

	// PE data
	wire		i_nop			= qout_int | qin_int;									// TODO, interrupts
	wire  [7:0]	i_imm			= (BOARD_IMM_MUX == 1'b0) ? SCR_OUT : I_PE_IMM;
	assign		PEInstruction	= {i_nop, PEInstructionFromMemory, i_imm};
	assign		dataToArray		= (I_IN_DATA_SEL == 2'b00) ? SCR_OUT : Q_IN;
	//assign		dataFromArray	= I_SCR_STORE_c ? (I_PE_DEST[5] ? dataInLeft : dataInRight) : dataFromArray;
    
    always @(posedge CLK, negedge RST_L) begin
        if(~RST_L) begin
            data_scr <= 0;
        end else begin
            if(I_PE_DEST[5]) begin
                data_scr <= dataInLeft;
            end else begin
                data_scr <= dataInRight;
            end
            //data_scr <= I_PE_DEST[5] ? dataInLeft : dataInRight;        
        end
    end
    
    assign dataFromArray = data_scr;
// instruction interlocks, controller pipeline
	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			controlInstructionFromMemoryB <= 44'd0;
			controlInstructionFromMemoryC <= 44'd0;
		end else begin
			controlInstructionFromMemoryC <= controlInstructionFromMemoryB;
			controlInstructionFromMemoryB <= controlInstructionFromMemoryA;
		end
	end

// module instantiations
	// Instantiated from ./cbs.v
	cbs ControllerBitShifter (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(RST_L),			// input  wire			RST_L,
		.K_W_OR_IN			(inWOR),			// input  wire			K_W_OR_IN,
		.I_CBS_SLEFT		(I_CBS_SLEFT_c),	// input  wire			I_CBS_SLEFT,
		.I_CBS_LOAD			(I_CBS_LOAD_c),		// input  wire			I_CBS_LOAD,
		.W_OR				(W_OR),				// output wire			W_OR,
		.BS_OUT				(BS_OUT)			// output reg	 [7:0]	BS_OUT
	);

	// Instantiated from ./scr.v
	scr ScratchReg (
		.CLK				(CLK),				// input  wire			CLK
		.RST_L				(RST_L),			// input  wire			RST_L
		.I_SCR_STORE		(I_SCR_STORE_c),	// input  wire			I_SCR_STORE
		.I_SCR_SEL			(I_SCR_SEL_c),		// input  wire [1:0]	I_SCR_SEL
		.ARRAY_DATA_IN		(dataFromArray),	// input  wire [7:0]	ARRAY_DATA_IN
		.Q_IN				(Q_IN),				// input  wire [7:0]	Q_IN
		.BS_OUT				(BS_OUT),			// input  wire [7:0]	BS_OUT
		.SCR_OUT			(SCR_OUT)			// output reg  [7:0]	SCR_OUT
	);

	// Instantiated from ./qout.v
	qout Qout (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(RST_L),			// input  wire			RST_L,
		.I_CBS_LOAD			(I_CBS_LOAD_c),		// input  wire			I_CBS_LOAD,
		.I_CBS_SLEFT		(I_CBS_SLEFT_c),	// input  wire			I_CBS_SLEFT,
		.I_DATA_WRITE		(I_DATA_WRITE_c),	// input  wire			I_DATA_WRITE,
		.I_FIFO_OUT			(I_FIFO_OUT),		// input  wire			I_FIFO_OUT,
		.SCR_OUT			(SCR_OUT),			// input  wire  [7:0]	SCR_OUT,
		.BS_OUT				(BS_OUT),			// input  wire  [7:0]	BS_OUT,
	    .out				(DATA_OUT),					// output wire			out,
		.qout_int			(qout_int)			// output wire			qout_int
	);

	// Instantiated from ./qin.v
	qin Qin (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(RST_L),			// input  wire			RST_L,
		.I_DATA_READ		(I_DATA_READ),		// input  wire			I_DATA_READ,
		.I_IN_DATA_SEL		(I_IN_DATA_SEL),	// input  wire  [1:0]	I_IN_DATA_SEL,
		.DATA_IN            (DATA_IN),          
		.Q_IN				(Q_IN),				// output wire  [7:0]	Q_IN,
		.qin_int			(qin_int)			// output wire			qin_int
	);

	// Instantiated from ./inst.v
	inst InstructionMemory (
		.CLK				(CLK),					// input  wire			CLK,
		.RST_L				(RST_L),				// input  wire			RST_L,
		.PC_OUT				(PC_OUT),				// input  wire [15:0]	PC_OUT,
		.instruction		(instructionFromMemory)	// output wire [95:0]	instruction
	);

	// NORA TODO: ??? Not sure why _c here (or not)
	// Instantiated from ./lc_stack.v
	lc_stack LoopCounter (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(RST_L),			// input  wire			RST_L,
		.SCR_OUT			(SCR_OUT),			// input  wire  [7:0]	SCR_OUT,
		.I_CNT_LOAD_0		(I_CNT_LOAD_c[0]),	// input  wire			I_CNT_LOAD_0,
		.I_CNT_LOAD_1		(I_CNT_LOAD_c[1]),	// input  wire			I_CNT_LOAD_1,
		.I_PUSH_CNT			(I_PUSH_CNT),		// input  wire			I_PUSH_CNT,
		.I_DEC_CNT			(I_DEC_CNT),		// input  wire			I_DEC_CNT,
		.D_Pop_Cnt			(D_Pop_Cnt),		// input  wire			D_Pop_Cnt,
		.I_IMM				(I_IMM),			// input  wire [15:0]	I_IMM,
		.Cntr_Branch_inv	(loopzero)			// output wire			Cntr_Branch
	);

	// Instantiated from ./pc_stack.v
	pc_stack ProgramCounter (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(RST_L),			// input  wire			RST_L,
		.I_POP_PC			(I_POP_PC),			// input  wire			I_POP_PC,
		.I_PUSH_PC			(I_PUSH_PC),		// input  wire			I_PUSH_PC,
		.I_PC_SEL			(I_PC_SEL),			// input  wire	 [1:0]	I_PC_SEL,
		.I_BR_0				(I_BR_0),			// input  wire			I_BR_0,
		.I_BR_W_OR			(I_BR_W_OR_c),		// input  wire			I_BR_W_OR,
		.W_OR				(W_OR),				// input  wire			W_OR,
		.Cntr_Branch_inv	(loopzero),			// input  wire			Cntr_Branch_inv,
		.I_IMM				(I_IMM),			// input  wire	[15:0]	I_IMM,
		.PC_OUT				(PC_OUT)			// output reg   [15:0]	PC_OUT
	);

endmodule: control
