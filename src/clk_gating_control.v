`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/07/2014 11:42:50 AM
// Design Name: 
// Module Name: clk_gating_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define OUT_OF_LOOP 2'b00
`define IN_FIRST_ITTR 2'b01
`define IN_LOOP 2'b10

`define MULT_FOUND 1'b0
`define MULT_FOUND_B 1'b1

`define ENABLE 1'b1
`define DISABLE 1'b0


module cg_control(
    input CLK,
    input RST_L,
    input I_PUSH_CNT,
    input I_DEC_CNT,
    input I_BR_0,
    input I_BR_W_OR_c,
    input [15:0] I_IMM,
    input [4:0] I_ALU_FUNC,
    
    output mult_en     
    );
    
    // Helper signals
    wire found_beginloop = ~I_PUSH_CNT & I_DEC_CNT & I_BR_0 & ~I_BR_W_OR_c;
    wire found_end_loop = ~I_PUSH_CNT & ~I_DEC_CNT & ~I_BR_0 & I_BR_W_OR_c;
    wire found_mult = I_ALU_FUNC[1];
    
    parameter stack_depth = 16;
    // State
    reg [1:0] loop_state;
    reg [1:0] loop_state_next;
    
    reg mult_state;
    reg mult_state_next;
    
    reg [15:0]  loop_count_stack[stack_depth-1:0];
    reg [1:0]   loop_state_stack[stack_depth-1:0];
    reg lc_stack_pointer;
    
    reg mult_en;
    
    always @(posedge CLK, negedge RST_L) begin
        if(~RST_L) begin
            mult_state <= MULT_FOUND;
            loop_state <= OUT_OF_LOOP; 
            lc_stack_pointer = 4'b0000;
            loop_counter_stack[4'b0000] = 4'hffff;
            loop_state_stack[4'b0000] = OUT_OF_LOOP;
        end else begin
            loop_state <= loop_state_next;
            mult_state <= mult_state_next;
        end
    end
    
    always @(found_beginloop, found_endloop) begin
        if(loop_state == OUT_OF_LOOP) begin
            if(found_beginloop) begin
                loop_state_next = IN_FIRST_ITTR;
                loop_counter_stack[lc_stack_pointer] = I_IMM;
            end
        end else begin
            if(loop_state == IN_FIRST_ITTR) begin
                if(found_endloop) begin
                    loop_state_next = IN_LOOP;
                    loop_counter_stack[lc_stack_pointer] <= loop_counter_stack[lc_stack_pointer] - 1'b1;
                end else begin
                    if(found_beginloop) begin
                        loop_counter_stack[lc_stack_pointer] = I_IMM;
                        loop_state_stack[lc_stack_pointer] = loop_state_next;
                        lc_stack_pointer <= lc_stack_pointer + 1'b1;
                    end
                end
            end else begin
                if(loop_state == IN_LOOP) begin
                    if(found_endloop) begin
                        loop_counter_stack[lc_stack_pointer] <= loop_counter_stack[lc_stack_pointer] - 1'b1;
                        if(loop_counter_stack[lc_stack_pointer] == 0) begin
                            lc_stack_pointer = lc_stack_pointer - 1'b1;
                            loop_state_next = loop_counter_stack[lc_stack_pointer];
                        end     
                    end else begin
                    end     
                end 
            end
        end
    end
    
    always @(mult_state) begin
        if(mult_state == MULT_FOUND) begin
            mult_en <= ENABLE;
        end else begin
            mult_en <= DISABLE;
        end
    end
    
endmodule
