`timescale 1ns / 1ps

module qin (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire			I_DATA_READ,
	input  wire  [1:0]	I_IN_DATA_SEL,
    input  wire  [7:0]  DATA_IN,
	output wire  [7:0]	Q_IN,
	output wire			qin_int
);

	wire read_en = I_DATA_READ & (I_IN_DATA_SEL == 2'b11);

	assign qin_int = read_en & empty;	// TODO: or (full and host write)

	queue_fifo Qin (
		.clk				(CLK),
		.rst				(~RST_L),
		.din				(DATA_IN),		// arbitrary (unused for now)
		.wr_en				(1'b1),		// arbitrary (unused for now)
		.rd_en				(read_en),
		.dout				(Q_IN),
		.full				(),
		.empty				(empty)
	);

endmodule: qin
