`timescale 1ns / 1ps

module scr (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire			I_SCR_STORE,
	input  wire [1:0]	I_SCR_SEL,
	input  wire [7:0]	ARRAY_DATA_IN,
	input  wire [7:0]	Q_IN,
	input  wire [7:0]	BS_OUT,			// AKA cbs

	output reg  [7:0]	SCR_OUT
);

	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			SCR_OUT <= 8'h00;
		end else begin
			if (I_SCR_STORE) begin
				case (I_SCR_SEL)
					2'b00: SCR_OUT <= ARRAY_DATA_IN;
					2'b01: SCR_OUT <= SCR_OUT;
					2'b10: SCR_OUT <= Q_IN;
					2'b11: SCR_OUT <= BS_OUT;
				endcase
			end
		end
	end

endmodule