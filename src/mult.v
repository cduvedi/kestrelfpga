`timescale 1ns / 1ps

/* mult.v
	Signed Multiplier with Double Accumulation
	Andrew W. Hill
	August 2006

	Two inputs are multiplied, then optionally, a third input
	and/or the high byte of the previous multiplication are
	added to the mult_result.

	Signed multiplication is handled using a 9x9 multiplier
	The MSB acts as a sign bit.  If the high byte of the input
	is "1", and the sign enable is "1", then the number is
	negative, and the sign is propagated to the 9th bit.
	If the number is not negative or not signed, a "0" will be
	carried into the top bit of the multiplier, so it will be
	treated as positive.  The bottom 8 bits are mlo, the next
	eight are mhi, and the top two are trash used for sign
	extension.

	The Spartan4 appears to have a multiply-single-accumulate
	built in as a primitive... sweet!  Used 14slices in 5-6ns.
																*/

module mult (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire  [4:0]	i_func,
	input  wire  [7:0]	opA,
	input  wire  [7:0]	opB,
	input  wire  [7:0]	opC,
	input  wire			mask,

	output wire  [7:0]	mult_result,
	output reg   [7:0]	mhi
);

// Signal instantiations
	wire		SA = i_func[1];
	wire		SB = i_func[2];
	wire		addmcEn = i_func[4];
	wire		addmhiEn = i_func[0];

// Multiplier
	wire [17:0]	tmp_result = ({SA&opA[7],opA} * {SB&opB[7],opB}) + (opC & {8{addmcEn}}) + (mhi & {8{addmhiEn}});;

	assign mult_result = tmp_result[7:0];	// lo byte

	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			mhi <= 8'h00;
		end else if (mask) begin
			mhi <= tmp_result[15:8];		// hi byte
		end else begin
			mhi <= mhi;
		end
	end

endmodule