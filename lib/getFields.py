from optparse import OptionParser

# Converts from Verilog index to Python index based on a 96-char instruction
def index(a):
	return 95-a

def temp_index(a):
	return 51-a

def fix_instruction_bin(binary):
	temp = binary[:index(44)+1]
	temp = temp[temp_index(51):temp_index(50)+1] + temp[temp_index(46)] + temp[temp_index(48):temp_index(47)+1] + temp[temp_index(49)] + temp[temp_index(45):temp_index(0)+1]
	return temp + binary[index(43):]

def prettyprint(line):

	# Get binary instruction
	binary = bin(int(line, 16))[2:].zfill(96)

	# Make correction of the PE instruction
	binary = fix_instruction_bin(binary)

	# Print out instruction encoding
	print("{")

	# Controller instruction encoding
	print("\tI_PC_SEL[1:0]:\t\t" + binary[index(1):index(0)+1])
	print("\tI_PUSH_PC:\t\t" + binary[index(2)])
	print("\tI_POP_PC:\t\t" + binary[index(3)])
	print("\tI_BR_0:\t\t\t" + binary[index(6)])
	print("\tI_BR_W_OR:\t\t" + binary[index(7)])
	print("\tI_PUSH_CNT:\t\t" + binary[index(4)])
	print("\tI_DEC_CNT:\t\t" + binary[index(5)])
	print("\tI_CNT_LOAD[1:0]:\t" + binary[index(9):index(8)+1])
	print()
	print("\tI_CBS_LOAD:\t\t" + binary[index(10)])
	print("\tI_CBS_SLEFT:\t\t" + binary[index(11)])
	print("\tI_SCR_SEL[1:0]:\t\t" + binary[index(13):index(12)+1])
	print("\tI_SCR_STORE:\t\t" + binary[index(14)])
	print()
	print("\tI_IN_DATA_SEL[1:0]:\t" + binary[index(16):index(15)+1])
	print("\tI_DATA_READ:\t\t" + binary[index(17)])
	print("\tI_DATA_WRITE:\t\t" + binary[index(18)])
	print("\tI_FIFO_OUT:\t\t" + binary[index(19)])
	print()
	print("\tI_BREAK:\t\t" + binary[index(20)])
	print("\tI_IMM[15:0]:\t\t" + binary[index(36):index(21)+1])
	print("\tD_pop_cnt:\t\t" + binary[index(37)])
	print("\tD_OUT[2:0:\t\t" + binary[index(40):index(38)+1])
	print("\tBOARD_IMM_MUX:\t\t" + binary[index(41)])
	print("\tI_SPARE[1:0]:\t\t" + binary[index(43):index(42)+1])
	print()

	# PE instruction encoding
	print("\timm_v2[7:0]:\t\t" + binary[index(51):index(44)+1])
	print("\tdest_v2[5:0]:\t\t" + binary[index(57):index(52)+1])
	print("\topC_v2[5:0]:\t\t" + binary[index(63):index(58)+1])
	print("\topA_v2[5:0]:\t\t" + binary[index(69):index(64)+1])
	print("\topB_v2[2:0]:\t\t" + binary[index(72):index(70)+1])
	print("\twr_v2:\t\t\t" + binary[index(73)])
	print("\trd_v2:\t\t\t" + binary[index(74)])
	print("\trm_v2[1:0]:\t\t" + binary[index(76):index(75)+1])
	print("\tbit_v2[3:0]:\t\t" + binary[index(80):index(77)+1])
	print("\tfb_v2[5:0]:\t\t" + binary[index(85):index(81)+1])
	print("\tfinv_v2:\t\t" + binary[index(86)])
	print("\tlc_v2:\t\t\t" + binary[index(87)])
	print("\tmp_v2:\t\t\t" + binary[index(88)])
	print("\tci_v2:\t\t\t" + binary[index(89)])
	print("\tfunc_v2[4:0]:\t\t" + binary[index(94):index(90)+1])
	print("\tforce_v2:\t\t" + binary[index(95)])
	print("}")

def main():

	# Parse arguments
	usage = "usage: %prog <FILENAME> [options]"
	parser = OptionParser(usage)
	parser.add_option("-t", "--time", action="store", type="int", dest="time")
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose")
	parser.add_option("-q", "--quiet", action="store_false", dest="verbose")
	(options, args) = parser.parse_args()

	# Interpret file argument
	if len(args) != 1:
		parser.error("Not enough arguments. You must at least specify a filename.")
	filename = args[0]
	if options.verbose:
		print("Reading %s..." % filename)
	f = open(filename)
	lines = f.readlines()

	# Interpret time option
	time = 0
	tmax = len(lines)
	if options.time:
		time = options.time
		if time >= tmax or time < 0:
			parser.error("Time option must be less than the number of instructions, i.e. within the interval [0," + str(tmax) + ")")
	else:
		print("Using default time = 0")

	# Interpret file
	line = lines[time].strip()
	print("Instruction " + str(time) + " : " + line)
	prettyprint(line)
	f.close()

if __name__ == '__main__':
	main()
